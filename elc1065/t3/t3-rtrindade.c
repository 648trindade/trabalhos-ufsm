#include <stdio.h>
#include <stdbool.h>
#include <math.h>

int main(){

    //dados
    double a=-1,b=-1,c=-1,A=-1,B=-1,C=-1,val;
    double A_cos,B_sen,A_sen;
    char dig;
    int nlv=0;
    int pronto=0;

    do{
        scanf(" %c%lf",&dig,&val);
        if (val<=0){
            printf ("Valor incorreto!\n");
        }
        else{
            nlv++;
            if (dig=='a'){
                if (a!=-1){   nlv--;}
                a=val;
            }
            else if (dig=='b'){
                if (b!=-1){   nlv--;}
                b=val;
            }
            else if (dig=='c'){
                if (c!=-1){   nlv--;}
                c=val;
            }
            else if (dig=='A'){
                if (A!=-1){  nlv--;}
                A=val;
            }
            else if (dig=='B'){
                if (B!=-1){   nlv--;}
                B=val;
            }
            else if (dig=='C'){
                if (C!=-1){   nlv--;}
                C=val;
            }
            else{
                nlv--;
                printf("Angulo ou lado \"%c\" inexistente!\n",dig);
            }
        }//fim else
    }while(nlv<3);

    while(pronto==0){
        //calcula 2º lado angulo
        if ((A>-1&&(B==-1&&C==-1))||(B>-1&&(A==-1&&C==-1))||(C>-1&&(A==-1&&B==-1))){
            printf("passou if 1 angulo\n");
            if (A>-1){   //se A
                if (a>-1){   //se Aa
                    if (b>-1)    B = (asin((b*sin(A*M_PI/180))/a)*180)/M_PI; //se Aab
                    else    C = (asin((c*sin(A*M_PI/180))/a)*180)/M_PI; //se Aac
                }
                else {  //se Abc
                    a = sqrt(pow(b,2) + pow(c,2) - (2*b*c*cos(A*M_PI/180)));
                    B = (asin((b*sin(A*M_PI/180))/a)*180)/M_PI;
                }
            }
            else if (B>-1){  //se B
                if (b>-1){   //se Bb
                    if (a>-1)    A = (asin((a*sin(B*M_PI/180))/b)*180)/M_PI; //se Bba
                    else    C = (asin((c*sin(B*M_PI/180))/b)*180)/M_PI; //se Bbc
                }
                else {  //se Bac
                    b = sqrt(pow(a,2) + pow(c,2) - (2*a*c*cos(B*M_PI/180)));
                    A = (asin((a*sin(B*M_PI/180))/b)*180)/M_PI;
                }
            }
            else {  //se C
                if (c>-1){   //se Cc
                    if (a>-1)    A = (asin((a*sin(C*M_PI/180))/c)*180)/M_PI; //se Cca
                    else    B = (asin((b*sin(C*M_PI/180))/c)*180)/M_PI; //se Ccb
                }
                else {  //se Cab
                    c = sqrt(pow(a,2) + pow(b,2) - (2*a*b*cos(C*M_PI/180)));
                    A = (asin((a*sin(C*M_PI/180))/c)*180)/M_PI;
                }
            }
        }

        //calcula 3º lado angulo
        else if((A>-1 && ((B>-1 && C==-1)||(B==-1 && C>-1)))||(A==-1 && B>-1 && C>-1)){
            printf("passou if 2 angulo\n");
            if (A==-1)   A=180-B-C;
            else if (B==-1)     B=180-A-C;
            else    C=180-A-B;
        }

        //nao tem nenhum angulo do triangulo
        else if (A==-1&&B==-1&&C==-1){
            printf("passou if sem angulo\n");
            A = (acos((pow(a,2)-pow(b,2)-pow(c,2))/((-2)*b*c))*180)/M_PI;
        }

        //calcula lado restante
        else {
            printf("passou if ultimo lado\n");
            if(a==-1&&b==-1&&c==-1)    printf("Impossivel calcular\n");
            else if (a==-1)   a = sqrt(pow(b,2) + pow(c,2) - (2*b*c*cos(A*M_PI/180)));
            else if (b==-1)   b = sqrt(pow(a,2) + pow(c,2) - (2*a*c*cos(B*M_PI/180)));
            if (c==-1)   a = sqrt(pow(a,2) + pow(b,2) - (2*a*b*cos(C*M_PI/180)));
        }

        if (A>-1&&B>-1&&C>-1&&a>-1&&b>-1&&c>-1) pronto=1;
        if (pronto==0) printf("mais um loop\n");
        else printf("deu\n");
    }

    printf("%.2lf %.2lf %.2lf %.2lf %.2lf %.2lf\n",A,B,C,a,b,c);
}
