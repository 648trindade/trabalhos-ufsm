#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include "t5-rtrindade.h"
#define H_SCREEN 640
#define W_SCREEN H_SCREEN*1.5
#define T_FON H_SCREEN/(32/3)

//nova variavel aleatoria
bool novo(int game[][4]){
     int i,j,pos;
     int x[16],y[16],cont=0;
     bool end=false;
     for (i=0;i<4;i++){
         for (j=0;j<4;j++){
             if (game[i][j]==0){
                x[cont]=i;
                y[cont]=j;
                cont++;
             }
         }
     }
     if (cont>0){      //Talvez d� pra tirar
        if (cont>1)    pos=(rand()%cont);
        else{
             pos=0;
             end=true;
        }
        int novo=rand()%5;
        if (novo==4)      game[x[pos]][y[pos]]=2;
        else              game[x[pos]][y[pos]]=1;
     return end;
     }
}

ALLEGRO_COLOR cor(int x){
    switch(x){
        case 0:
            return al_map_rgb(205,193,180);
            break;
        case 1:
            return al_map_rgb(238,228,218);
            break;
        case 2:
            return al_map_rgb(237,224,200);
            break;
        case 4:
            return al_map_rgb(242,177,121);
            break;
        case 8:
            return al_map_rgb(245,149,99);
            break;
        case 16:
            return al_map_rgb(250,75,18);
            break;
        case 32:
            return al_map_rgb(255,0,0);
            break;
        case 64:
            return al_map_rgb(255,0,255);
            break;
        case 128:
            return al_map_rgb(255,0,255);
            break;
        case 256:
            return al_map_rgb(255,0,255);
            break;
        case 512:
            return al_map_rgb(255,0,255);
            break;
    }
}

void mostrar(int game[][4],int placar){
     ALLEGRO_FONT *fonte = NULL, *fonte2 = NULL;
     fonte = al_load_font("FONTE.otf",T_FON,0);
     fonte2 = al_load_font("FONTE.otf",T_FON-10,0);
     int bloco = (H_SCREEN-20)/4;
     int i,j,x1,x2,y1,y2;
     char num[5],score[40];
     al_clear_to_color(al_map_rgb(187,173,160));
     for (i=0;i<4;i++){
         for (j=0;j<4;j++){
             x1=4+(j*(bloco+4));
             y1=4+(i*(bloco+4));
             x2=x1+bloco;
             y2=y1+bloco;
             sprintf(num,"%d",game[i][j]);
             al_draw_filled_rectangle(x1, y1, x2, y2, cor(game[i][j]));
             if (game[i][j]!=0)
                al_draw_text(fonte, al_map_rgb(0,0,0), x1+(bloco/2), y1+(bloco/2)-(T_FON*0.755/2), ALLEGRO_ALIGN_CENTRE, num);
             else
                al_draw_text(fonte, al_map_rgb(0,0,0), x1+(bloco/2), y1+(bloco/2)-(T_FON*0755/2), ALLEGRO_ALIGN_CENTRE, " ");
         }
     }
     sprintf(score,"Pontuacao: %d",placar);
     al_draw_text(fonte2,al_map_rgb(0,0,0),W_SCREEN/1.2, 10, ALLEGRO_ALIGN_CENTRE, score);
     al_draw_text(fonte2,al_map_rgb(0,0,0),W_SCREEN*2/3+10, (H_SCREEN/2+(T_FON-10))+2, ALLEGRO_ALIGN_LEFT, "[S] Salvar");
     al_draw_text(fonte2,al_map_rgb(0,0,0),W_SCREEN*2/3+10, (H_SCREEN/2+(T_FON-10)*2)+4, ALLEGRO_ALIGN_LEFT, "[L] Carregar");
     al_draw_text(fonte2,al_map_rgb(0,0,0),W_SCREEN*2/3+10, (H_SCREEN/2+(T_FON-10)*3)+6, ALLEGRO_ALIGN_LEFT, "[Q] Sair");
     al_flip_display();
}

bool venceu(int game[][4],int placar){
     int i,j;
     for (i=0;i<4;i++){
         for (j=0;j<4;j++){
             if (game[i][j]==512){
                 ALLEGRO_FONT *fonte = NULL;
                 fonte = al_load_font("FONTE.otf",T_FON,0);
                 al_draw_text(fonte, al_map_rgb(0,0,0), W_SCREEN/1.2, H_SCREEN-62, ALLEGRO_ALIGN_CENTRE, "Voce Venceu! :D");
                 al_flip_display();
                 al_rest(4);
                 return true;
             }
         }
     }
     return false;
}

void save(int game[][4],int score){
    FILE *savef;
    int i,j;
    savef=fopen("save","w");
    for (i=0;i<4;i++){
        for (j=0;j<4;j++)
            fprintf(savef,"%d ",game[i][j]);
    }
    fprintf(savef,"%d",score);
    fclose(savef);
    ALLEGRO_FONT *fonte = NULL;
    fonte = al_load_font("FONTE.otf",T_FON,0);
    al_draw_text(fonte, al_map_rgb(0,0,0), W_SCREEN/1.2, (H_SCREEN/2)-30, ALLEGRO_ALIGN_CENTRE, "Jogo salvo!");
    al_flip_display();
    al_rest(1);
}
int load(int game[][4]){
    FILE *loadf;
    int i,j,score;
    loadf=fopen("save","r");
    for (i=0;i<4;i++){
        for (j=0;j<4;j++)
            fscanf(loadf,"%d",&game[i][j]);
    }
    fscanf(loadf,"%d",&score);
    fclose(loadf);
    ALLEGRO_FONT *fonte = NULL;
    fonte = al_load_font("FONTE.otf",T_FON,0);
    al_draw_text(fonte, al_map_rgb(0,0,0), W_SCREEN/1.2, (H_SCREEN/2)-30, ALLEGRO_ALIGN_CENTRE, "Carregado!");
    al_flip_display();
    al_rest(1);
    return score;
}

int main(){
       ALLEGRO_DISPLAY *display = NULL;
       ALLEGRO_EVENT_QUEUE *evento = NULL;
       ALLEGRO_EVENT ev;

       al_init();
       al_init_primitives_addon();
       al_install_mouse();
       al_init_font_addon();
       al_init_ttf_addon();
       al_install_keyboard();

       display = al_create_display(W_SCREEN, H_SCREEN);

       evento = al_create_event_queue();
       al_register_event_source(evento, al_get_display_event_source(display));
       al_register_event_source(evento, al_get_keyboard_event_source());

       int game[4][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
       bool end=false,gera=true;
       char cmd;
       int placar=0,bug=0;

       end = novo(game);

       while (!end){
             if(gera)
                 end = novo(game);
             gera = true;
             mostrar(game,placar);
             if (!end){
                do{
                       al_wait_for_event(evento, &ev);
                       if(ev.type == ALLEGRO_EVENT_KEY_UP){
                            if(ev.keyboard.keycode==ALLEGRO_KEY_LEFT)
                                cmd='a';
                            else if (ev.keyboard.keycode==ALLEGRO_KEY_UP)
                                cmd='w';
                            else if (ev.keyboard.keycode==ALLEGRO_KEY_RIGHT)
                                cmd='d';
                            else if (ev.keyboard.keycode==ALLEGRO_KEY_DOWN)
                                cmd='s';
                            else if (ev.keyboard.keycode==ALLEGRO_KEY_Q){
                                cmd=0;
                                end=true;
                                break;
                            }
                            else if (ev.keyboard.keycode==ALLEGRO_KEY_S){
                                cmd=0;
                                save(game,placar);
                                break;
                            }
                            else if (ev.keyboard.keycode==ALLEGRO_KEY_L){
                                cmd=0;
                                placar = load(game);
                                break;
                            }

                       }
                       else cmd=0;
                }while (!(cmd=='a' || cmd=='w' || cmd =='s' || cmd=='d'));

                placar = altera(cmd,game,placar,&gera);
                if (!end)
                    end = venceu(game,placar);
             }
             else{
                 ALLEGRO_FONT *fonte = NULL;
                 fonte = al_load_font("FONTE.otf",T_FON,0);
                 al_draw_text(fonte, al_map_rgb(0,0,0), W_SCREEN/1.2, H_SCREEN-62, ALLEGRO_ALIGN_CENTRE, "Voce Perdeu! :(");
                 al_flip_display();
                 al_rest(4);
             }
       }
       al_destroy_display(display);

}
