#define cheio_vazio (!(temp[x][0]==0&&temp[x][1]==0&&temp[x][2]==0&&temp[x][3]==0))||(!(temp[x][0]!=0)&&(temp[x][1]!=0)&&(temp[x][2]!=0)&&(temp[x][3]!=0))
bool ordenado(int temp[]){
    if (((temp[0]!=0)&&(temp[1]==0)&&(temp[2]==0)&&(temp[3]==0)))  return true; //se | x | _ | _ | _ |
    else if (((temp[0]!=0)&&(temp[1]!=0)&&(temp[2]==0)&&(temp[3]==0))) return true; //se | x | x | _ | _ |
    else if ((temp[0]!=0)&&(temp[1]!=0)&&(temp[2]!=0)) return true;//se | x | x | x | _ |
    else if ((temp[0]==0)&&(temp[1]==0)&&(temp[2]==0)&&(temp[3]==0)) return true;//se | _ | _ | _ | _ |
    return false;
}

int processa(int temp[][4], int placar, int x, int *soma){
    int j;
    bool ready=false;

    if (cheio_vazio){
       //enquanto nao tiver ordenado
        while (1){
            //se | x | x | x | _ |*/
            if(ordenado(temp[x]))  break;

            for (j=0;j<3;j++){
                if (temp[x][j]==0){
                    (*soma)++;
                    temp[x][j]=temp[x][j+1];
                    temp[x][j+1]=0;
                }
            }
        }
    }//fim se, tudo ordenado
    while (!(ready)){
        if (temp[x][0]==temp[x][1]&&temp[x][0]!=0){
            temp[x][0]*=2;
            placar+=temp[x][0];
            temp[x][1]=0;
            (*soma)++;
        }
        else if (temp[x][1]==temp[x][2]&&temp[x][1]!=0){
            temp[x][1]*=2;
            placar+=temp[x][1];
            temp[x][2]=temp[x][3];
            temp[x][3]=0;
            (*soma)++;
            break;
        }
        else if (temp[x][2]==temp[x][3]&&temp[x][2]!=0){
            if (temp[x][1]==0){
                temp[x][1]=temp[x][2]*2;
                placar+=temp[x][1];
                temp[x][2]=0;
            }
            else{
                temp[x][2]*=2;
                placar+=temp[x][2];
            }
            temp[x][3]=0;
            (*soma)++;
            break;
        }
        else if (temp[x][1]==0&&temp[x][2]!=0){
            temp[x][1]=temp[x][2];
            temp[x][2]=temp[x][3];
            temp[x][3]=0;
            (*soma)++;
            break;
        }

        else         ready=true;
    }
    return placar;
}

void transpoe(char cmd, int temp[][4]){
    int i,j,temp2[4][4];
    if (cmd=='w' || cmd=='s'){
        for (i=0;i<4;i++){
            for (j=0;j<4;j++)
                temp2[i][j]=temp[j][i];
        }
        for (i=0;i<4;i++){
            for (j=0;j<4;j++)
                temp[i][j]=temp2[i][j];
        }
    }
}

void copia(char cmd, int mat1[][4], int mat2[][4]){
    int i;
    for (i=0;i<4;i++){
        if (cmd=='a' || cmd=='w'){
            int j;
            for (j=0;j<4;j++)
                mat1[i][j]=mat2[i][j];
        }
        else if (cmd=='s' || cmd=='d'){
            mat1[i][0]=mat2[i][3];
            mat1[i][1]=mat2[i][2];
            mat1[i][2]=mat2[i][1];
            mat1[i][3]=mat2[i][0];
        }
    }
}

int altera(char cmd, int game[][4], int placar, bool *gera){
    int i,temp[4][4],temp_s[4][4],soma=0;
    bool ready;

    if (cmd=='s'){
        copia('a',temp_s,game);
        transpoe(cmd,temp_s);
        copia(cmd,temp,temp_s);
    }
    else if(cmd!=0){
        copia(cmd,temp,game);
        transpoe(cmd,temp);
    }

    for (i=0;i<4;i++){
        if (cmd=='a' || cmd=='w' || cmd =='s' || cmd=='d'){
            placar=processa(temp,placar,i,&soma);
        }
    }
    if (soma==0)   *gera=false;

    if (cmd=='s'){
        copia(cmd,temp_s,temp);
        transpoe(cmd,temp_s);
        copia('a',game,temp_s);
    }
    else if(cmd!=0){
        transpoe(cmd,temp);
        copia(cmd,game,temp);
    }

    return placar;
}
