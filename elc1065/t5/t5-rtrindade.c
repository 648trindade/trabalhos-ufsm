#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "t5-rtrindade.h"

//nova variavel aleatoria
bool novo(int game[][4]){
    int i,j,pos;
    int x[16],y[16],cont=0;
    bool end=false;
    for (i=0;i<4;i++){
        for (j=0;j<4;j++){
            if (game[i][j]==0){
                x[cont]=i;
                y[cont]=j;
                cont++;
            }
        }
    }
    if (cont>0){      //Talvez dê pra tirar
        if (cont>1)    pos=(rand()%cont);
        else{
            pos=0;
            end=true;
        }
        int novo=rand()%5;
        if (novo==4)      game[x[pos]][y[pos]]=2;
        else              game[x[pos]][y[pos]]=1;
        return end;
    }
}

void mostrar(int game[][4],int placar){
    int i,j;
    for (i=0;i<4;i++){
        printf("\n\n|");
        for (j=0;j<4;j++)
            if (game[i][j]!=0)
                printf("\t%d\t|",game[i][j]);
            else
                printf("\t\t|");
    }
    printf("\n\n\n\t\t\t\tPontuacao: %d\n",placar);
}

int load(int game[][4]){
    FILE *loadf;
    int i,j,score;
    loadf=fopen("save","r");
    for (i=0;i<4;i++){
        for (j=0;j<4;j++)
            fscanf(loadf,"%d",&game[i][j]);
    }
    fscanf(loadf,"%d",&score);
    fclose(loadf);
    printf("\n\t\t\t\tCarregado!");
    return score;
}

void save(int game[][4],int score){
    FILE *savef;
    int i,j;
    savef=fopen("save","w");
    for (i=0;i<4;i++){
        for (j=0;j<4;j++)
            fprintf(savef,"%d ",game[i][j]);
    }
    fprintf(savef,"%d",score);
    fclose(savef);
    printf("\n\t\t\t\tSalvo!");
}




bool venceu(int game[][4],int placar){
    int i,j;
    for (i=0;i<4;i++){
        for (j=0;j<4;j++){
            if (game[i][j]==512){
                printf("\n\n\n\t\t\t\tVoce Venceu! :D\n");
                //getch();
                scanf("%d",&j);
                return true;
            }
        }
    }
    return false;
}


int main(){
    int game[4][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
    bool end=false,gera=true;
    char cmd='r';
    int placar=0;

    end = novo(game);
    while (!end){
        if(gera)
            end = novo(game);
        gera = true;
        mostrar(game,placar);
        if (!end){
            do{
                printf("[M] Menu [WASD] Move: ");
                scanf(" %c",&cmd);
            }while (!(cmd=='a' || cmd=='w' || cmd =='s' || cmd=='d' || cmd=='m'));
            if (cmd=='m'){
                printf("[L] Carregar [S] Salvar [C] Continuar [E] Sair: ");
                while(!(cmd== 'l' || cmd=='s' || cmd=='e' || cmd=='c'))
                    scanf(" %c",&cmd);
                if (cmd=='l')
                    load(game);
                else if(cmd=='s')
                    save(game,placar);
                else if(cmd=='e')
                    exit(0);
                gera = false;
            }
            else{
                placar = altera(cmd,game,placar,&gera);
                if(venceu(game,placar))
                    end=true;
            }
        }
        else{
            printf("\n\n\n\t\t\t\tVoce perdeu! :(\n");
            scanf(" %c",&cmd);
        }
    }
    return 0;
}
