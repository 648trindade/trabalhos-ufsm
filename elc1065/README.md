# Laboratório de Programação I

**Código:** ELC1065

**Período:** 2014/1

**Professor:** Benhur de O. S.

## Lista de trabalhos

1. Cálculo de Triângulos
2. Cálculo de Triângulos (condicionais)
3. Cálculo de Triângulos (laços + condicionais)
4. Jogo da Velha 3D
5. Jogo 512 (c/ e sem Allegro5)
6. Círculos Mortais

## Requisitos para compilação
* make
* gcc
* liballegro5-dev