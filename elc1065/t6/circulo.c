#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#define H_SCREEN 600
#define W_SCREEN 1024

//cria estrututa ponto
typedef struct {
    float x,y;
} ponto;

//cria estrutura circulo
typedef struct {
    ponto centro;
    float raio;
    ponto vel;
    ALLEGRO_COLOR cor;
} circulo;

//calcula distancia entre centros
float distancia(ponto p1, ponto p2){
    return sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2));
}

//determina uma nova velocidade com base na posicao inicial da bola na tela
float velocidade(float p, int res){
    float vel = (rand()%9)+1;
    if (p>=res/2)   vel*=-1;
    return vel;
}

//reinicia o círculo para algum lugar fora da tela
circulo resetcirculo(circulo c, float rpl){
    int canto = rand()%4;
    int intervalo = rpl*5 - rpl*0.25;
    c.raio = (rand()%intervalo)+rpl*0.25;

    c.cor = al_map_rgb(rand()%256, rand()%256, rand()%256);
    switch (canto){
        case 0: //começa em cima
                c.centro.x = rand()%W_SCREEN;
                c.centro.y = 0 - c.raio;
                break;
        case 1: //começa a direita
                c.centro.y = rand()%H_SCREEN;
                c.centro.x = W_SCREEN + c.raio;
                break;
        case 2: //começa em baixo
                c.centro.x = rand()%W_SCREEN;
                c.centro.y = H_SCREEN + c.raio;
                break;
        default: //começa a esquerda
                c.centro.y = rand()%H_SCREEN;
                c.centro.x = 0 - c.raio;
                break;
    }
    c.vel.x = velocidade(c.centro.x,W_SCREEN);
    c.vel.y = velocidade(c.centro.y,H_SCREEN);
    return c;
}

//testa se o circulo saiu pra fora da tela
bool foradatela(circulo c){
    if (c.vel.x>=0 && c.centro.x-c.raio>=W_SCREEN)
        return true;
    else if (c.vel.x<0 && c.centro.x+c.raio<0)
        return true;
    else if (c.vel.y>=0 && c.centro.y-c.raio>=W_SCREEN)
        return true;
    else if (c.vel.y<0 && c.centro.y+c.raio<0)
        return true;

    return false;
}

//testa se há colisão entre circulos
bool colide (circulo c1, circulo c2){
    if (distancia(c1.centro,c2.centro)>c1.raio+c2.raio)
        return false;
    else
        return true;
}

//função principal
int main(){

    //criação das varivaeis allegro
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_EVENT_QUEUE *evento = NULL;
    ALLEGRO_TIMER *timer = NULL;
    ALLEGRO_FONT *fonte = NULL, *fonte2 = NULL, *fonte3 = NULL;
    ALLEGRO_EVENT ev,ev2,welcome;
    ALLEGRO_BITMAP *circle = NULL;

    //inicializações allegro
    al_init();
    al_init_primitives_addon();
    al_install_mouse();
    al_install_keyboard();
    al_init_font_addon();
    al_init_ttf_addon();
    al_init_image_addon();

    //cria um contador e uma tela
    timer = al_create_timer(1.0 / 100);
    display = al_create_display(W_SCREEN, H_SCREEN);
    fonte = al_load_font("FONTE.ttf",60,0);
    fonte3 = al_load_font("FONTE.ttf",30,0);
    fonte2 = al_load_font("FONTE2.ttf",30,0);
    circle = al_load_bitmap("back.bmp");

    //cria e registra evento na tela, contador e mouse (esconde o mouse)
    evento = al_create_event_queue();
    al_register_event_source(evento, al_get_display_event_source(display));
    al_register_event_source(evento, al_get_timer_event_source(timer));
    al_register_event_source(evento, al_get_mouse_event_source());
    al_register_event_source(evento, al_get_keyboard_event_source());
    al_start_timer(timer); //inicializa contador
    al_hide_mouse_cursor(display);


    srand(time(NULL)); //inicializa gerador nos aleatorios
    int i, j, balls=50, scr,rec=0;
    circulo c[balls], pl; //inicializa circulos e circulo jogador
    float a;
    ponto mou;
    char score[20],recor[20];// rec[4];
    bool redraw=true,end,start=false,fim=false,new_record=false;

    //carrega record salvo
    FILE *record;
    record = fopen("record","r+");
    if (record){
        fscanf(record,"%d",&rec);
        fclose(record);
    }
    else    rec=0;


    //imprime a tela de boas vindas
    al_clear_to_color(al_map_rgb(0,0,0));
    al_draw_bitmap(circle, 0,0,0);
    al_draw_bitmap(circle, 675,0,0);
    al_draw_text(fonte, al_map_rgb(255,255,255), W_SCREEN/2, H_SCREEN/2-90, ALLEGRO_ALIGN_CENTRE, "CIRCULOS DA MORTE");
    al_draw_text(fonte3, al_map_rgb(255,255,255), W_SCREEN/2, H_SCREEN/2-30, ALLEGRO_ALIGN_CENTRE, "PRESS. BOTAO ESQ. PARA REINICIAR");
    al_draw_text(fonte3, al_map_rgb(255,255,255), W_SCREEN/2, H_SCREEN/2, ALLEGRO_ALIGN_CENTRE, "PRESS. BOTAO DIR. PARA SAIR");
    al_draw_text(fonte2, al_map_rgb(255,255,255), W_SCREEN/2, H_SCREEN/2+70, ALLEGRO_ALIGN_CENTRE, "CLIQUE PARA COMECAR!");
    al_flip_display();

    //espera enquanto não se é clicado, para começar
    while (!start){
        al_wait_for_event(evento, &welcome);
        if (welcome.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP){
          start=true;
          pl.centro.x = welcome.mouse.x ;
          pl.centro.y = welcome.mouse.y ;
        }
    }

    //começa o loop de jogos
    while(!fim){
        start = true;
        end=false;
        new_record = false;
        scr=0;

        //raio e cor do jogador
        pl.raio=10;
        pl.cor = al_map_rgb(255, 255, 255);

        //gera circulos
        for (i=0;i<balls;i++)
            c[i] = resetcirculo(c[i],pl.raio);

        //inicia um jogo
        while(!end){

            //cria um evento
            al_wait_for_event(evento, &ev);

            //testa pro pause
            if(ev.type == ALLEGRO_EVENT_KEY_DOWN){
                if (ev.keyboard.keycode == ALLEGRO_KEY_SPACE)
                    redraw = !redraw;
            }

            //testa se o display não é fechado
            else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
                 fim = true;
                 break;
            }
            //testa se o evento é um movimento do mouse
            else if(ev.type == ALLEGRO_EVENT_MOUSE_AXES || ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY) {
                  if (start){
                    pl.centro.x = H_SCREEN/2;
                    pl.centro.y = W_SCREEN/2;
                    start = false;
                  }
                  else{
                    pl.centro.x = ev.mouse.x ;
                    pl.centro.y = ev.mouse.y ;
                  }
            }
            //testa se o evento é um clique do mouse (esq. reinicia, dir. fecha o jogo)
            else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
                if (ev.mouse.button==2)
                    fim = true;
              break;
            }

            //depois que analisa todos os eventos
            if(redraw && al_is_event_queue_empty(evento)) {
                al_clear_to_color(al_map_rgb(0,0,0)); //limpa a tela
                //loop dentro dos circulos
                for (i=0;i<balls;i++){
                    //circulo sai da tela
                    if(foradatela(c[i]))
                        c[i] = resetcirculo(c[i],pl.raio);
                    //colisão com o jogador
                    if (colide(c[i],pl)){
                        a = M_PI * (pow(c[i].raio,2)+pow(pl.raio,2));
                        if (pl.raio>=c[i].raio){
                            pl.raio = sqrt(a/M_PI); //novo tamanho pro jogador
                            c[i] = resetcirculo(c[i],pl.raio); //reinicia circulo
                            scr++;
                                //seta novo recorde
                            if (scr>rec){
                                rec = scr;
                                new_record = true;
                            }
                        }
                        else{       //se o circulo for maior
                            end=true; //seta fim do jogo
                            al_clear_to_color(al_map_rgb(0,0,0)); //limpa a tela pra preto
                            al_draw_filled_circle(c[i].centro.x, c[i].centro.y, c[i].raio, al_map_rgb(255,0,0));
                            al_draw_filled_circle(pl.centro.x, pl.centro.y, pl.raio, al_map_rgb(200,0,0));

                            if (!new_record)
                                al_draw_text(fonte, al_map_rgb(255,255,255), W_SCREEN/2, H_SCREEN/2-30, ALLEGRO_ALIGN_CENTRE, "PERDEU!");
                            else
                                al_draw_text(fonte, al_map_rgb(255,255,255), W_SCREEN/2, H_SCREEN/2-30, ALLEGRO_ALIGN_CENTRE, "NOVO RECORDE!");

                            sprintf(score,"SCORE: %d",scr);
                            al_draw_text(fonte, al_map_rgb(255,255,255), W_SCREEN/2, H_SCREEN/2+30, ALLEGRO_ALIGN_CENTRE, score);
                            al_flip_display();
                            ev2.type = ALLEGRO_EVENT_TIMER;
                            while(ev2.type != ALLEGRO_EVENT_MOUSE_BUTTON_UP){
                                al_wait_for_event(evento, &ev2);
                                if(ev2.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP){
                                    if (ev2.mouse.button==2)
                                        fim = true;
                                    pl.centro.x = ev2.mouse.x ;
                                    pl.centro.y = ev2.mouse.y ;
                                    break;
                                }
                            }
                        }
                    }
                    al_draw_filled_circle(c[i].centro.x, c[i].centro.y, c[i].raio, c[i].cor); //imprime circulo atual
                    c[i].centro.x+=c[i].vel.x; //atualiza o valor dos pontos centrais
                    c[i].centro.y+=c[i].vel.y;
                }
                al_draw_filled_circle(pl.centro.x, pl.centro.y, pl.raio, pl.cor); //imprime o jogador
                sprintf(score,"SCORE: %d",scr);
                sprintf(recor,"RECORD: %d",rec);
                al_draw_text(fonte2, al_map_rgb(255,255,255), W_SCREEN-40, H_SCREEN-32, ALLEGRO_ALIGN_RIGHT, score);
                al_draw_text(fonte2, al_map_rgb(230,230,230), W_SCREEN-40, H_SCREEN-62, ALLEGRO_ALIGN_RIGHT, recor);
                if(!end) al_flip_display(); //mostra tudo na tela
                al_rest(0.05); //espera
            }
        }
    }

    al_destroy_display(display);
    //salva o recorde atual
    record = fopen("record","w");
    fprintf(record,"%d",rec);
    fclose(record);
}
