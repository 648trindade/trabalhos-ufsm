#include <stdio.h>

int main(){
    char tab[3][3][3]={{{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}},{{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}},{{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}}};
    char mar;
    int fim = 0;
    int t,l,c,i,j,rodada=0;

    while (fim==0){
        //escolhe X ou O
        rodada++;
        if (rodada%2!=0)  mar='X';
        else    mar='O';

        //pede linha e coluna
        printf ("\n\nTabuleiro, Linha e coluna:\n");
        scanf ("%d%d%d",&t,&l,&c);
        if (tab[l][t][c]=='X'||tab[l][t][c]=='O')
            printf("\nPosicao ja setada!");
        else{
            tab[l][t][c]=mar;

            //imprime tabela
            for (i=0;i<3;i++){
                printf("\n");
                for (j=0;j<3;j++){
                    printf("%c | %c | %c\t",tab[i][j][0],tab[i][j][1],tab[i][j][2]);
                }
            }

            //testa vitoria
            for (i=0;i<3;i++){
                for (j=0;j<3;j++){
                    //vitoria em linha
                    if ((tab[j][i][0]==tab[j][i][1]&&tab[j][i][0]==tab[j][i][2]&&tab[j][i][0]!=' ')||
                        (tab[0][i][j]==tab[1][i][j]&&tab[0][i][j]==tab[2][i][j]&&tab[0][i][j]!=' ')||
                        (tab[i][0][j]==tab[i][1][j]&&tab[i][0][j]==tab[i][2][j]&&tab[i][0][j]!=' ')){
                        printf ("\n\nVit�ria de %c!\n",mar);
                        fim=1;
                    }
                }//vitoria em faixa na mesma tabela
                if ((tab[0][i][0]==tab[1][i][1]&&tab[0][i][0]==tab[2][i][2]&&tab[0][i][0]!=' ')||
                    (tab[0][i][2]==tab[1][i][1]&&tab[0][i][2]==tab[2][i][0]&&tab[0][i][2]!=' ')){
                    printf ("\n\nVit�ria de %c!\n",mar);
                    fim=1;
                }
                //vitoria da linha em 3D
                else if((tab[i][0][0]==tab[i][1][1]&&tab[i][0][0]==tab[i][2][2]&&tab[i][0][0]!=' ')||
                        (tab[0][0][i]==tab[1][1][i]&&tab[0][0][i]==tab[2][2][i]&&tab[0][0][i]!=' ')){
                        printf ("\n\nVit�ria de %c!\n",mar);
                        fim=1;
                }
            }
            //vitoria em faixa atravessando tabelas
            if ((tab[0][0][0]==tab[1][1][1]&&tab[0][0][0]==tab[2][2][2]&&tab[0][0][0]!=' ')||
                (tab[0][0][2]==tab[1][1][1]&&tab[0][0][2]==tab[2][2][0]&&tab[0][0][2]!=' ')||
                (tab[0][2][0]==tab[1][1][1]&&tab[0][2][0]==tab[2][0][2]&&tab[0][2][0]!=' ')||
                (tab[0][2][2]==tab[1][1][1]&&tab[0][2][2]==tab[2][0][0]&&tab[0][2][2]!=' ')){
                printf ("\n\nVit�ria de %c!\n",mar);
                fim=1;
            }
        }
    }
}
