#include<stdio.h>
#include<math.h>

int main(){
    //dados
    double a,b,c;
    double A,B,C;
    double B_sen,A_cos;

    /*---------------------------ABb----------------------*/

    //entrada
    scanf("%lf%lf%lf",&A,&B,&b);

    //calculo
    C = 180 - A - B;
    a = b/sin(B*M_PI/180)*sin(A*M_PI/180);
    c = b/sin(B*M_PI/180)*sin(C*M_PI/180);

    //saída
    printf("%.2lf %.2lf %.2lf\n",C,a,c);

    /*------------------------ABc-------------------------*/

    //entrada
    scanf("%lf%lf%lf",&A,&B,&c);

    //calculo
    C = 180 - A - B;
    a = c/sin(C*M_PI/180)*sin(A*M_PI/180);
    b = c/sin(C*M_PI/180)*sin(B*M_PI/180);

    //saída
    printf("%.2lf %.2lf %.2lf\n",C,a,b);

   /*------------------------Abc-------------------------*/

    //entrada
    scanf("%lf%lf%lf",&A,&b,&c);

    //calculo
    //a² = b²+c²-2*b*c*cos A
    a = sqrt(pow(b,2) + pow(c,2) - (2*b*c*cos(A*M_PI/180)));
    /* a/sen A = b/sen B
    sen B = (b* sen A)/a */
    B_sen = (b*sin(A*M_PI/180))/a;
    //B = arcsen(sen B)*180 / pi
    B = (asin(B_sen)*180)/M_PI;
    C = 180 - A - B;

    //saída
    printf("%.2lf %.2lf %.2lf\n",B,C,a);

    /*--------------------------AB-----------------------*/

    //entrada
    scanf("%lf%lf%lf",&A,&a,&b);

    //calculo
    /* a/sen A = b/sen B
    sen B = (b* sen A)/a */
    B_sen = (b*sin(A*M_PI/180))/a;
    //B = arcsen(sen B)*180 / pi
    B = (asin(B_sen)*180)/M_PI;
    C = 180 - A - B;
    //c² = a²+b²-2*a*b*cos C
    c = sqrt(pow(a,2) + pow(b,2) - (2*a*b*cos(C*M_PI/180)));

    //saída
    printf("%.2lf %.2lf %.2lf\n",A,B,C);

    /*--------------------------Bc-----------------------*/

    //entrada
    scanf("%lf%lf%lf",&a,&b,&c);

    //calculo
    //cos A = (a²-b²-c²)/(-2*b*c)
    A_cos = (pow(a,2)-pow(b,2)-pow(c,2))/((-2)*b*c);
    //A = arccos(cos A)*180 / pi
    A = (acos(A_cos)*180)/M_PI;

    /* a/sen A = b/sen B
    sen B = (b* sen A)/a */
    B_sen = (b*sin(A*M_PI/180))/a;
    //B = arcsen(sen B)*180 / pi
    B = (asin(B_sen)*180)/M_PI;

    C = 180 - A - B;

    //saída
    printf("%.2lf %.2lf %.2lf\n",A,B,C);

}
