# Trabalhos UFSM

**Autor:** Rafael Gauna Trindade

**Email:** rtrindade at inf.ufsm.br

## Tabela de Disciplinas
| Código  | Disciplina                    |
| ------- | ----------------------------- |
| ELC1065 | Laboratório de Programação I  |
| ELC1067 | Laboratório de Programação II |
