#ifndef _PILHA_H_
#define _PILHA_H_

/*
 * pilha.h
 * TAD que implementa uma pilha de elementos.
 * (c) Joao V. Lima, UFSM, 2014.
 * (c) Benhur Stein, UFSM, 2005.
 */

#include <stdbool.h>

/* definicao do tipo pilha */
typedef struct pilha{
    void* dado;
    struct pilha *prox;
} pilha_t;

/* funcoes que permitem realizar operacoes sobre uma pilha */

/* retorna uma nova pilha, vazia */
pilha_t *pilha_cria(void);

/* destroi a pilha p, que devera estar vazia. */
void pilha_destroi(pilha_t *p);

/* retorna true se a pilha p estiver vazia. */
bool pilha_vazia(pilha_t *p);

/* insere o dado do tipo void* na pilha p */
void pilha_insere(pilha_t *p, void *dados);

/* remove e retorna o elemento no topo da pilha */
void *pilha_remove(pilha_t *p);

/* retorna true se p for uma pilha válida */
bool pilha_valida(pilha_t *p);

void* pilha_mostra_topo(pilha_t *p);
int pilha_tamanho(pilha_t *p);

#endif /* _PILHA_H_ */
