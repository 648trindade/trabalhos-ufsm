#include <stdio.h>
#include <stdlib.h>
#include "geom.h"
#include "tela.h"
#include "calc.h"
#include <string.h>
#include <stdbool.h>

double strToDouble(char str[]){
    int i, dot = strlen(str);
    double d=0, j=1.0;
    for(i=0;i<strlen(str);i++)
        if (str[i]=='.'){
            dot = i;
            break;
        }
    for(i=dot-1;i>=0;i--){
        if ((str[i]<'0'||str[i]>'9') && !(i==0 && str[i]=='-'))
            return false;
        if (i==0 && str[i]=='-')
            d *= -1;
        else{
            d += (str[i]-48)*j;
            j *= 10;
        }
    }
    if (dot!=strlen(str)){
        j=10.0;
        for (i=dot+1;i<strlen(str);i++){
            if (str[i]<'0'||str[i]>'9')
                return false;
            d += (str[i]-48)/j;
            j *= 10;
        }
    }
    return d;
}

bool charInStr(char c, char str[]){
    int i;
    for (i=0;i<strlen(str);i++)
        if (str[i]=='.')
            return true;
    return false;
}

bool cliqueBotao(tela_t *tela, retangulo_t botao[], char vBotao[][6], calc_t *c, char conta[], char valor[]){
    ponto_t mouse = {0,0};
    if (tela_botao(tela))
        mouse = tela_rato(tela);
    int key = tela_tecla(tela), i;
    bool key_press = true;
    char bt, temp[2] = " ";
    double n;
    if (key == 0)
        key_press = false;
    for (i=0; i<20; i++){
        bt = vBotao[i][0];
        temp[0] = bt;
        if (ptemret(mouse, botao[i])){
            if (bt>='0' && bt<='9' && strlen(conta)<=39){
                strcat(valor,temp);
                strcat(conta,temp);
            }
            else if (bt=='.'){
                if (!charInStr(bt,valor) && strlen(valor)>0 && strcmp(valor,"-")!=0 && strlen(conta)<=39){
                    strcat(valor,temp);
                    strcat(conta,temp);
                }
            }
            else if (bt=='+' || bt=='*' || bt=='-' || bt=='/' || bt==')'){
                if ((strlen(valor)>0 && strcmp(valor,"-")!=0) || conta[strlen(conta)-1]==')'){
                    if (!(bt==')' && c->parAbertos==0)){
                        if (conta[strlen(conta)-1]!=')'){
                            n = strToDouble(valor);
                            calc_operando(c, n);
                        }
                        calc_operador(c, bt);
                        strcat(conta,temp);
                        strcpy(valor,"");
                    }
                }
                else if (strlen(valor)==0 && bt=='-'){
                    strcat(valor,temp);
                    strcat(conta,temp);
                }
            }
            else if (bt=='('){
                if (strlen(valor)==0){
                    calc_operador(c, bt);
                    strcat(conta,temp);
                }
            }
            else if (bt=='='){
                if ((strlen(valor)>0 && strcmp(valor,"-")!=0 && c->parAbertos==0) ||\
                    (c->parAbertos==0 && conta[strlen(conta)-1]==')')){
                    if (conta[strlen(conta)-1]!=')'){
                        n = strToDouble(valor);
                        calc_operando(c, n);
                    }
                    strcpy(conta,"");
                    calc_fim(c);
                    sprintf(conta,"%.4f",calc_resultado(c));
                    strcpy(valor,conta);
                    calc_inicializa(c);
                }
            }
            else if (bt=='C'){
                strcpy(valor,"");
                strcpy(conta,"");
                calc_inicializa(c);
            }
            else if (bt=='O'){
                return true;
            }
        }
    }
    return false;
}

void desenhaCalculadora(tela_t *tela, retangulo_t display, retangulo_t botao[], char vBotao[][6], char conta[]){
    ponto_t ponto;
    cor_t preto = {0,0,0};
    cor_t cinza = {200,200,200};
    cor_t vermelho = {255,200,200};
    cor_t cinzaEscuro = {150,150,150};
    cor_t azul = {200,200,255};
    tamanho_t tam;
    /*Desenha Display*/
    tela_cor(tela, azul);
    tela_retangulo(tela, display);
    tam = tela_tamanho_texto(tela,conta);
    ponto.x = display.pos.x + 5;
    ponto.y = display.pos.y + display.tam.alt/2 - tam.alt/2;
    tela_cor(tela,preto);
    tela_texto(tela, ponto, conta,2);
    /*Desenha Botões*/
    int i;
    char bt;
    for(i=0; i<20; i++){
        bt = vBotao[i][0];
        if (bt>='0' && bt<='9' || bt=='.')
            tela_cor(tela, cinza);
        else if (bt=='C'||bt=='O')
            tela_cor(tela, vermelho);
        else if (bt=='=')
            tela_cor(tela, azul);
        else
            tela_cor(tela, cinzaEscuro);
        tela_retangulo(tela, botao[i]);
        tam = tela_tamanho_texto(tela,vBotao[i]);
        ponto.x = botao[i].pos.x + botao[i].tam.larg/2 - tam.larg/2;
        ponto.y = botao[i].pos.y + botao[i].tam.alt/2 - tam.alt/2;
        tela_cor(tela,preto);
        tela_texto(tela, ponto, vBotao[i],2);
    }
}


void criaJanela(tela_t *tela, retangulo_t *display, retangulo_t botao[], calc_t *c){
    tamanho_t size = {800,600};
    tela_inicializa(tela, size, "Calculadora");
    c = calc_cria();
    (*display).pos.x = 5;
    (*display).pos.y = 5;
    (*display).tam.larg = 790;
    (*display).tam.alt  = 90;
    int i,j,k;
    for (i=0; i<4; i++){
        for (j=0; j<5; j++){
            k = 4*i + j + i;
            botao[k].pos.x = 5*(j+1) + j*154;
            botao[k].pos.y = 95 + 4*(i+1) + i*120;
            botao[k].tam.larg = 154;
            botao[k].tam.alt = 120;
        }
    }
}

int main()
{
    tela_t* tela = (tela_t*)malloc(sizeof(tela_t));
    calc_t* c = (calc_t*)malloc(sizeof(calc_t));
    retangulo_t display, botao[20];
    char valor[39] = "", conta[39] = "";
    char vBotao[20][6] = {"9", "8", "7", "CLEAR", "OFF",\
                          "6", "5", "4", "(", ")",\
                          "3", "2", "1", "/", "*",\
                          ".", "0", "=", "-", "+"};
    criaJanela(tela, &display, botao, c);
    calc_inicializa(c);
    while(1){
        tela_limpa(tela);
        if (cliqueBotao(tela, botao, vBotao, c, conta, valor))
            break;
        desenhaCalculadora(tela, display, botao, vBotao, conta);
        tela_mostra(tela);
        tela_espera(20);
    }
    calc_destroi(c);
    tela_finaliza(tela);
    return 0;
}
