#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"
#include "calc.h"

// cria uma nova calculadora, retorna um ponteiro para ela.
calc_t *calc_cria(void){
    calc_t* c = (calc_t*)malloc(sizeof(calc_t));
    c->operadores = NULL;
    c->operandos = NULL;
    c->res = 0;
    c->parAbertos = 0;
    return c;
};

// destroi uma calculadora, libera a memoria ocupada por ela.
void calc_destroi(calc_t *c){
    pilha_destroi(c->operadores);
    pilha_destroi(c->operandos);
    free(c);
}

// inicializa a calculadora para novos calculos,
// abandona eventuais calculos pendentes
void calc_inicializa(calc_t *c){
    /*if (c->operadores!= NULL)
        pilha_destroi(c->operadores);
    if (c->operandos!= NULL)
        pilha_destroi(c->operandos);*/
    c->operadores = pilha_cria();
    c->operandos = pilha_cria();
    c->res = 0;
    c->parAbertos = 0;
}

// realiza um calculo
void calc_calcula(calc_t *c, double val2, bool val2und);

// abre parenteses
void calc_abreparen(calc_t *c){
    char* ins = (char*)malloc(sizeof(char));
    *ins = '(';
    pilha_insere(c->operadores, ins);
    c->parAbertos++;
}

// fecha parenteses
void calc_fechaparen(calc_t *c){
    while(*((char*)pilha_mostra_topo(c->operadores))!='(')
          calc_calcula(c,0,true);
    free(pilha_remove(c->operadores));
    if (!pilha_vazia(c->operandos) && !pilha_vazia(c->operadores))
        if (*((char*)pilha_mostra_topo(c->operadores))=='*' || *((char*)pilha_mostra_topo(c->operadores))=='/')
            calc_calcula(c, 0, true);
    c->parAbertos--;
}

// insere um novo operador para o calculo (pode ser '+', '-', '*', '/', '^')
void calc_operador(calc_t *c, char operador){
    if (operador == '(')
        calc_abreparen(c);
    else if (operador == ')' && c->parAbertos>0)
        calc_fechaparen(c);
    else{
        char* ins = (char*)malloc(sizeof(char));
        *ins = operador;
        pilha_insere(c->operadores, ins);
    }
}

/*Realiza os calculos e faz as modificações nas pilhas
val2und:    define se val2 precisa ser acessado na pilha ou não*/
void calc_calcula(calc_t *c, double val2, bool val2und){
    double val1, *res;
    void* dado;
    char op = *((char*)pilha_mostra_topo(c->operadores));
    if (val2und){
        dado = pilha_remove(c->operandos);
        if (pilha_vazia(c->operandos)){
            pilha_insere(c->operandos, dado);
            pilha_destroi(c->operandos);
            pilha_destroi(c->operadores);
            c->operadores = pilha_cria();
            c->operandos = pilha_cria();
            calc_operador(c,'(');
            return;
        }
        else
            pilha_insere(c->operandos, dado);
        val2 = *((double*)pilha_mostra_topo(c->operandos));
        dado = pilha_remove(c->operandos);
        val1 = *((double*)pilha_mostra_topo(c->operandos));
        pilha_insere(c->operandos, dado);
    } else
        val1 = *((double*)pilha_mostra_topo(c->operandos));
    res = (double*)malloc(sizeof(double));
    /*Testa se o primeiro numero é negativo (precedido de um '-'), se for,
    o negativa e inverte o '-' da pilha pra '+'*/
    dado = pilha_remove(c->operadores);
    if(!pilha_vazia(c->operadores))
        if (*((char*)pilha_mostra_topo(c->operadores))=='-'){
            char *add = (char*)malloc(sizeof(char));
            *add = '+';
            pilha_remove(c->operadores);
            pilha_insere(c->operadores, add);
            val1 = -val1;
        }
    pilha_insere(c->operadores,dado);
    switch(op){
        case '+':
            *res = val1 + val2;  break;
        case '-':
            *res = val1 - val2;  break;
        case '*':
            *res = val1 * val2;  break;
        case '/':
            *res = val1 / val2;  break;
    }
    free(pilha_remove(c->operadores));
    free(pilha_remove(c->operandos));
    if (val2und)
        free(pilha_remove(c->operandos));
    pilha_insere(c->operandos, res);
}

// insere um novo operando para o calculo
void calc_operando(calc_t *c, double operando){
    if (!pilha_vazia(c->operandos) && (*((char*)pilha_mostra_topo(c->operadores))=='*' || *((char*)pilha_mostra_topo(c->operadores))=='/'))
        calc_calcula(c, operando, false);
    else{
        double* ins = (double*)malloc(sizeof(double));
        *ins = operando;
        pilha_insere(c->operandos, ins);
    }
}

// retorna o resultado calculo (topo da pilha de calculo)
// esse resultado sera parcial se ainda nao foi chamado calc_fim().
double calc_resultado(calc_t *c){
    return c->res;
}

// finaliza o calculo, retorna false se erro detectado
bool calc_fim(calc_t *c){
    if (pilha_vazia(c->operandos))
        return false;
    else if (pilha_tamanho(c->operandos)>1 && !pilha_vazia(c->operadores)){
        while (!pilha_vazia(c->operadores) && pilha_tamanho(c->operandos)>1){
            calc_calcula(c,0,true);
        }
        if (!pilha_vazia(c->operadores) || pilha_tamanho(c->operandos)>1)
            return false;
        else{
            c->res = *((double*)pilha_mostra_topo(c->operandos));
            return true;
        }
    }
    else if (!pilha_vazia(c->operandos)){

        if (pilha_tamanho(c->operandos)>1 || !pilha_vazia(c->operadores))
            return false;
        else{
            c->res = *((double*)pilha_mostra_topo(c->operandos));
            return true;
        }
    }
    else
        return false;
}
