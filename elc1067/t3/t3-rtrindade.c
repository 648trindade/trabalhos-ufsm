#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <locale.h>
#include <string.h>
#include <time.h>

/*LEGENDA:
0 - SEM BOMBA, OCULTO
1 - COM BOMBA, OCULTO
2 - SEM BOMBA, RELEVADO
3 - COM BOMBA, REVELADO
*/
int imprime(int m, int n, int bomb, char **game, int time0){
    system("clear");
    int i,j,contO=0,contB=0;
    bool lose = false;

    printf("LEGENDA:   # = Oculto\n\
             = Vazio\n\
           P = Bandeira\n\
           Q = Mina\n");

    printf("\n\t");

    //Imprime nros colunas
    for(i=0;i<n;i++)
        printf("%d    ",i+1);
    printf("\n");
    for(i=0;i<m;i++){
        //imprime nro linha
        printf("\n%d\t",i+1);
        for (j=0;j<n;j++){
            //imprime posição
            if (game[i][j]<'0'){
                printf("P    ");
                contB++;
                contO++;
            }
            else if (game[i][j]<'2'){
                printf("#    ");
                contO++;
            }
            else if (game[i][j]=='2')
                printf("     ");
            else if (game[i][j]>'3')
                printf("%c    ",game[i][j]-3);
            else{
                printf("Q    ");
                lose=true;
            }
        }
    }
    //relatorios
    printf("\n\nBandeiras: %d/%d",contB,bomb);
    printf("\t\tPontuação: %d",time(0)-time0);
    //se encontrou todas as bombas
    if (!lose && contO==bomb)     return 1;
    else if (!lose){
        printf("\nProbabilidade de encontrar uma bomba: %.2f\%\n",bomb*100.0/contO);
        return 0;
    }
    else if (lose)           return 2;

}

void venceu(){
    char com;
    printf("\nVocê Venceu!\n\nPressione 'Q' para sair ou 'N' para um novo jogo: ");
    while(1){
        scanf(" %c",&com);
        if (com=='q' || com=='Q')
            exit(0);
        else if (com == 'n' || com == 'N')
            break;
    }

}

void perdeu(){
    char com;
    printf("\nVocê Perdeu!\n\nPressione 'Q' para sair ou 'N' para um novo jogo: ");
    while(1){
        scanf(" %c",&com);
        if (com=='q' || com=='Q')
            exit(0);
        else if (com == 'n' || com == 'N')
            break;
    }

}

void revela(char** game, int x, int y, int m, int n){
    int cont=0,i,j;

    for (i=-1;i<2;i++){
        for (j=-1;j<2;j++){

            if ((i==0 && j==0) || (x+i<0 || x+i>=m || y+j<0 || y+j>=n))
                continue;
            else if(game[x+i][y+j]=='1')
                cont++;
        }
    }

    if (cont>0)
        game[x][y]='3'+cont;
        //printf("\nCom número %d %d",x,y);
    else{
        for (i=-1;i<2;i++)
            for (j=-1;j<2;j++){
                if ((i==0 && j==0) || (x+i<0 || x+i>=m || y+j<0 || y+j>=n))
                    continue;
                else if(game[x+i][y+j]=='0'){
                    game[x+i][y+j]='2';
                    revela(game,x+i,y+j,m,n);
                }
            }
        game[x][y]='2';
        //printf("\nSem número %d %d",x,y);
    }
}

bool joga(char **game, int m, int n, int i){
    int j;
    char k;
    scanf("%d",&j);
    if (i>0 && i<=m && j>0 && j<=n)
        if (game[i-1][j-1]<'2')
            game[i-1][j-1]+=2;
    if (game[i-1][j-1]=='2')
        revela (game,i-1,j-1,m,n);
    else if (game[i-1][j-1]=='3'){
        for (i=0;i<m;i++)
            for (j=0;j<n;j++)
                if (game[i][j]=='1')
                    game[i][j]='3';
    }
}

void bandeira(char **game, int m, int n){
    int i,j;
    scanf("%d%d",&i,&j);
    if (game[i-1][j-1]<'0')
        game[i-1][j-1]+=2;
    else if (game[i-1][j-1]<'2')
        game[i-1][j-1]-=2;
}

int main(){

    setlocale(LC_ALL, "Portuguese");

    char **game;
    char com[4];
    int i,j,m,n,b,bomb;
    int time0;
    srand(time(NULL));


    while (1){
        system("clear");
        printf("\nInsira Largura, Comprimento e Qtd. de Bombas: ");
        scanf("%d%d%d",&m,&n,&bomb);
        game = (char **) malloc(m*sizeof(char *));

        for (i=0;i<m;i++){
            game[i] = (char *) malloc(n*sizeof(char *));
            for (j=0;j<n;j++){
                    game[i][j] = '0';
            }
        }

        b=0;
        while(b<bomb){
            i = rand()%m;
            j = rand()%n;
            if (game[i][j]=='0'){
                game[i][j]='1';
                b++;
            }
        }

        time0 = time(0);
        while(1){

            i = imprime(m,n,bomb,game,time0);
            if (i == 0){

                printf("\nPressione 'Q' para sair OU 'N' para um novo jogo\nOU 'P' seguido de coordenadas (X,Y) para inserir/remover bandeira\nOU insira a posição da jogada (X,Y): ");
                scanf("%s",&com);

                if (strcmp(com,"q")==0 || strcmp(com,"Q")==0)
                    exit(0);

                else if (strcmp(com,"n")==0 || strcmp(com,"N")==0)
                    break;
                else if (strcmp(com,"p")==0 || strcmp(com,"P")==0)
                    bandeira(game,m,n);

                else {
                    i = atoi(com);
                    joga(game,m,n,i);
                }

            }
            else if (i == 1){
                venceu();
                break;
            }
            else{
                perdeu();
                break;
            }
        }
        for (i=0;i<m;i++)
            free(game[i]);
        free(game);
    }

    return 0;
}
