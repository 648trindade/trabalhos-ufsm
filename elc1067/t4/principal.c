
/* principal.c - arquivo da main.
 * Esse programa testa as TADs implementadas.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

/* Inclui definições de TADs. */
#include "vetor.h"
#include "geom.h"

int main(int argc, char** argv)
{
	const int npontos = 20;
	int i;
	vetor_t *vet_pontos = NULL;
	ponto_t *p1 = NULL;
	ponto_t *p2 = NULL;
	float distancia = 0.0f;

	/* semente para números aleatórios */
	srand(time(NULL));
	/* inicializa TAD vetor */
	vet_pontos = vetor_cria();

	/* inicia valores */
	for(i= 0; i < npontos; i++){
		p1 = (ponto_t*) malloc(sizeof(ponto_t));
		p1->x = (1.0 * (rand()%100)) / 33.3;
		p1->y = (1.0 * (rand()%100)) / 33.3;
		vetor_insere(vet_pontos, i, p1);
		printf("ponto insere %d %.2f %.2f (total %d)\n",
			i, p1->x, p1->y, vetor_numelem(vet_pontos));
	}

	/* calcula distancias entre pontos [0,n) */
	for(i= 0; i < (npontos-1); i++){
		p1 = (ponto_t*)vetor_acessa(vet_pontos, i);
		p2 = (ponto_t*)vetor_acessa(vet_pontos, i+1);
		if((p1 == NULL) || (p2 == NULL)){
			printf("ERRO ao acessar vetor (pos=%d)\n", i);
			exit(-1);
		}
		distancia = geom_distancia(*p1, *p2);
		printf("pontos distancia p1 %d %.2f %.2f p2 %d %.2f %.2f valor %.2f\n",
			i, p1->x, p1->y, i+1, p2->x, p2->y, distancia);
	}

	/* remove valores e libera */
	for(i= 0; i < npontos; i++){
		p1 = (ponto_t*)vetor_remove(vet_pontos, i);
		if(p1 == NULL){
			printf("ERRO ao remover de vetor (pos=%d)\n", i);
			exit(-1);
		}
		printf("ponto remove %d %.2f %.2f (total %d)\n", i, p1->x, p1->y,
			vetor_numelem(vet_pontos));
		free(p1);
	}
	vetor_destroi(vet_pontos);

	return 0;
}
