/* 
* As duas linhas seguintes, e a ultima do arquivo, servem para que este arquivo
* nao seja incluido mais de uma vez
*/
#ifndef GEOM_H
#define GEOM_H

/* geom.h
 ------
 declaracoes de tipos de dados e funcoes para manipulacao de formas
 geometricas simples (ponto, retangulo, circulo)
 */

#include <stdbool.h>

/* Tipos de dados */

/* Um ponto, constituído por uma coordenada no eixo x e no eixo y */
typedef struct {
    float x;
    float y;
} ponto_t;

/* tamanho_t, constituído por altura e largura, do tipo float */
typedef struct {
    float larg;
    float alt;
} tamanho_t;

/* retangulo_t, constituído por um ponto (que contém as coordenadas do canto 
              inferior esquerdo do retangulo) e um tamanho */
typedef struct {
    ponto_t pos;
    tamanho_t tam;
} retangulo_t;

/* circulo_t, constituido por um ponto (o centro do circulo) e um float (o raio) */
typedef struct {
    ponto_t centro;
    float raio;
} circulo_t;


/* Funções */

/* retorna a distancia entre dois pontos */
float geom_distancia(ponto_t p1, ponto_t p2);

/* retorna true se o ponto estiver dentro do circulo, false caso contrario */
bool geom_ptemcirc(ponto_t p, circulo_t c);

/* retorna true se o ponto estiver dentro do retangulo */
bool geom_ptemret(ponto_t p, retangulo_t r);

/* retorna true se houver uma interseccao entre o circulo e o retangulo */
bool geom_intercr(circulo_t c, retangulo_t r);

/* retorna true se houver uma interseccao entre os dois retangulos */
bool geom_interrr(retangulo_t r1, retangulo_t r2);

/* retorna true se houver uma interseccao entre os dois circulos */
bool geom_intercc(circulo_t c1, circulo_t c2);

#endif /* GEOM_H */
