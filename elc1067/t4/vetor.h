#ifndef _VETOR_H_
#define _VETOR_H_

/*
 * vetor.h
 * TAD que implementa um vetor dinamico.
 *
 * (c) Joao V. Lima, UFSM, 2014.
 * (c) Benhur Stein, UFSM, 2005.
 */

#include <stdbool.h>

/* definicao do tipo vetor */
typedef struct vetor vetor_t;

/* funções que permitem realizar operações sobre um vetor */

/* retorna um novo vetor, vazio */
vetor_t* vetor_cria(void);

/* destrói o vetor vet, que deverá estar vazio. */
void vetor_destroi(vetor_t* vet);

/* retorna o número de elementos no vetor vet. */
int vetor_numelem(vetor_t *vet);

/* insere o dado dado no vetor vet, na posição indice.
   o elemento já existente nessa posição e os elementos subsequentes, avançam
   uma posição */
void vetor_insere(vetor_t *vet, int indice, void *dado);

/* remove e retorna o dado na posição indice do vetor vet. */
void *vetor_remove(vetor_t *vet, int indice);

/* retorna o dado na posição indice do vetor vet */
void *vetor_acessa(vetor_t *vet, int indice);

/* retorna true se o vetor vet for válido */
bool vetor_valido(vetor_t *vet);

#endif
