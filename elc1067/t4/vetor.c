/*
 * vetor.c
 * TAD que implementa um vetor dinamico.
 *
 * (c) Joao V. Lima, UFSM, 2014.
 * (c) Benhur Stein, UFSM, 2005.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "vetor.h"

struct vetor {
	/* defina os campos da TAD vetor aqui */
    void **dados;
	int n;		/* número de elementos */
	int tam;    /* tamanho alocado do vetor */
};

vetor_t* vetor_cria(void)
{
	struct vetor *vet = (struct vetor*)malloc(sizeof(struct vetor));
	vet->n = 0;
	vet->tam = 0;
	vet->dados = NULL;
	return vet;
}

void vetor_destroi(vetor_t* vet)
{
	free(vet);
}

int vetor_numelem(vetor_t *vet)
{
	return vet->n;
}

void vetor_insere(vetor_t *vet, int indice, void *dado)
{
	if (indice >= vet->tam){
	    vet->dados = realloc(vet->dados,(indice+1)*sizeof(dado));
	    if (vet->dados != NULL)
            vet->tam++;
	    else{
	        free(vet);
            printf("\nErro ao (re)alocar memória\n");
            exit(1);
	    }
    }
    vet->dados[indice] = dado;
    vet->n++;
}

void *vetor_remove(vetor_t *vet, int indice)
{
	void *p;
	p = vet->dados[indice];
	if (p==NULL)
        return NULL;
    else{
        vet->n--;
        return p;
    }
}

void *vetor_acessa(vetor_t *vet, int indice)
{
	if (vet->dados[indice]==NULL)
        return NULL;
    else{
        return vet->dados[indice];
    }
}

bool vetor_valido(vetor_t *vet)
{
	if (vet!=NULL)
        return true;
    else
        return false;
}
