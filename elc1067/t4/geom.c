
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "geom.h"


float geom_distancia(ponto_t p1, ponto_t p2)
{
	return sqrt(pow((p2.x-p1.x),2)+pow((p2.y-p1.y),2));
}

bool geom_ptemcirc(ponto_t p, circulo_t c)
{
	if (geom_distancia(p, c.centro)>c.raio)
        return false;
    else
        return true;
}

bool geom_ptemret(ponto_t p, retangulo_t r)
{
	if ((p.x<=r.pos.x+r.tam.larg)&&(p.y<=r.pos.y+r.tam.alt))
        return true;
    else
        return false;
}

bool geom_intercr(circulo_t c, retangulo_t r)
{
	ponto_t lim;
	if (geom_ptemret(c.centro, r))
        return true;
    else{
        if (c.centro.x < r.pos.x)
            lim.x = r.pos.x - c.centro.x;
        else if (c.centro.x > r.pos.x+r.tam.larg)
            lim.x = c.centro.x - r.pos.x+r.tam.larg;
        else
            lim.x = 0;

        if (c.centro.y < r.pos.y)
            lim.y = r.pos.y - c.centro.y;
        else if (c.centro.y > r.pos.y+r.tam.alt)
            lim.y = c.centro.y - r.pos.y+r.tam.alt;
        else
            lim.y = 0;

        if (geom_distancia(c.centro, lim)<c.raio)
            return true;
        else
            return false;
    }
}

bool geom_interrr(retangulo_t r1, retangulo_t r2)
{
	ponto_t _r1,_r2;
	_r1.x = r1.pos.x + r1.tam.larg;
	_r1.y = r1.pos.y + r1.tam.alt;
	_r2.x = r2.pos.x + r2.tam.larg;
	_r2.y = r2.pos.y + r2.tam.alt;

	if (geom_ptemret(r1.pos,r2) || geom_ptemret(r2.pos,r1) || geom_ptemret(_r1,r2) || geom_ptemret(_r2,r1))
        return true;
    else
        return false;
}

bool geom_intercc(circulo_t c1, circulo_t c2)
{
	if (geom_distancia(c1.centro, c2.centro)>c1.raio+c2.raio)
        return true;
    else
        return false;
}
