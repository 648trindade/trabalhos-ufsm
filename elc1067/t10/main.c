#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"

int main(int argc, char** argv)
{
    arvore_t *a = arv_cria();
    int l=0;
    char p[255];
    FILE* f = fopen(argv[1],"r");
    if(f!=NULL){
        while(fscanf(f,"%s",p)!=EOF){
            a = arv_insere(a,l,p);
            l++;
        }
        fclose(f);
        arv_imprime(a);
        printf("\n");
        a = arv_destroi(a);
        arv_imprime(a);
    }

    return 0;
}
