#include "lista.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct tree{
    char *palavra;
    lista_t *linhas;
    struct tree *esq, *dir;
}arvore_t;

arvore_t* arv_cria();

arvore_t* arv_busca_pai(arvore_t* a, arvore_t* n);

arvore_t* arv_busca(arvore_t *a, char* bus);

arvore_t* arv_insere(arvore_t* a, int l, char* p);

arvore_t* arv_remove(arvore_t* a, char *p);

arvore_t* arv_destroi(arvore_t* a);

void arv_imprime(arvore_t* a);

bool arv_vazia(arvore_t* a);
