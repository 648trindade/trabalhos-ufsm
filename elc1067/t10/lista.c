#include "lista.h"

lista_t* lis_cria(){
    return NULL;
}

lista_t* lis_insere(lista_t* l, int dado){
    if (l==NULL){
        l = (lista_t*)malloc(sizeof(lista_t));
        l->dado = dado;
        l->prox = NULL;
        l->ant = NULL;
        return l;
    }
    lista_t *i;
    for (i=l;i->prox!=NULL;i=i->prox);
    i->prox = (lista_t*)malloc(sizeof(lista_t));
    i->prox->dado = dado;
    i->prox->prox = NULL;
    i->prox->ant = i;
    return l;
}

void lis_destroi(lista_t* l){
    if(l!=NULL){
        lis_destroi(l->prox);
        lis_remove(l,l->dado);
    }
}

void lis_remove(lista_t* l, int dado){
    lista_t *i;
    for (i=l;i->dado!=dado && i->prox!=NULL;i=i->prox);
    if (i->dado==dado && i->prox!=NULL){
        i->ant->prox = i->prox;
        i->prox->ant = i->ant;
        free(&(i->dado));
        free(i);
    }
    else if(i->dado==dado && i->prox==NULL){
        if(i->ant!=NULL)
            i->ant->prox = NULL;
        //i->prox = NULL;
        //free(&(i->dado));
        free(i);
    }
}

void lis_imprime(lista_t* l){
    while(l!=NULL){
        printf("%d, ",l->dado);
        l = l->prox;
    }
}

bool lis_vazia(lista_t* l){
    if (l==NULL)
        return true;
    return true;
}
