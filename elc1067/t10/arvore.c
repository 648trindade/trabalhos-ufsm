#include "arvore.h"

arvore_t* arv_cria(){
    return NULL;
}

arvore_t* arv_busca(arvore_t *a, char* bus){
    if (a==NULL){
        //printf("BUSCA %d\n",a);
        return NULL;
    }
    else{
        //printf("BUSCA %d %s\n",a,a->palavra);
        if(strcmp(bus,a->palavra)>0)
            return arv_busca(a->dir, bus);
        else if (strcmp(bus,a->palavra)<0)
            return arv_busca(a->esq, bus);
        else
            return a;
    }
}

arvore_t* arv_busca_pai(arvore_t* a, arvore_t* n){
    if (a==n || a==NULL)
        return NULL;
    else{
        if (a->dir==n || a->esq==n)
            return a;
        else if (strcmp(n->palavra,a->palavra)>=0)
            return arv_busca_pai(a->dir, n);
        else if (strcmp(n->palavra,a->palavra)<0)
            return arv_busca_pai(a->esq, n);
        else
            return NULL;
    }
}

arvore_t* arv_insere(arvore_t* a, int l, char p[]){
    if(a==NULL){
        a = (arvore_t*)malloc(sizeof(arvore_t));
        //a->palavra = (char*)malloc(sizeof(p));
        a->palavra = (char*)malloc(strlen(p)+1);
        strcpy(a->palavra, p);
        a->linhas = lis_cria();
        a->linhas = lis_insere(a->linhas, l);
        a->esq = NULL;
        a->dir = NULL;
        return a;
    }
    int r = strcmp(p,a->palavra);
    if (r>0)
        a->dir = arv_insere(a->dir,l,p);
    else if (r<0)
        a->esq = arv_insere(a->esq,l,p);
    else
        a->linhas = lis_insere(a->linhas, l);
    return a;
}

arvore_t* arv_remove(arvore_t* a, char *p){
    arvore_t* r = arv_busca(a,p);
    if (r==NULL){
        //printf(">>> Nada a ser feito\n");
        return NULL;
    }
    else{
        arvore_t* pr = arv_busca_pai(a,r), *i, *pi;
        lista_t *swapl;
        //printf(">> PROCESSO DE REMOCAO %s\n",r->palavra);
        char *swapp;
        if(r->dir==NULL && r->esq==NULL){
            //printf(">> No folha\n");
            lis_destroi(r->linhas);
            free(r->palavra);
            free(r);
            if(pr!=NULL){
                //printf(">> Seu Pai era: %s\n",pr->palavra);
                if(pr->esq==r)
                    pr->esq = NULL;
                else
                    pr->dir = NULL;
            }
            else{
                //printf(">> Era Raiz e folha\n");
                return NULL;
            }
        }
        else if (r->dir==NULL){
            //printf(">> Sem filhos a direita\n");
            if(pr!=NULL){
                if(pr->esq==r)
                    pr->esq = r->esq;
                else if(pr->dir==r)
                    pr->dir = r->esq;
                lis_destroi(r->linhas);
                free(r->palavra);
                free(r);
            }
            else{
                //printf(">> No raiz\n");
                swapp = r->palavra;
                r->palavra = r->esq->palavra;
                //printf(">> substituido por %s\n",r->palavra);
                free(swapp);
                swapl = r->linhas;
                r->linhas = r->esq->linhas;
                free(swapl);
                r->dir = r->esq->dir;
                i = r->esq;
                r->esq = r->esq->esq;
                free(i);
            }
        }
        else if(r->esq==NULL){
            //printf(">> Sem filhos a esquerda\n");
            if(pr!=NULL){
                if(pr->esq==r)
                    pr->esq = r->dir;
                else if(pr->dir==r)
                    pr->dir = r->dir;
                lis_destroi(r->linhas);
                free(r->palavra);
                free(r);
            }
            else{
                ////printf(">> No raiz\n");
                swapp = r->palavra;
                r->palavra = r->dir->palavra;
                //printf(">> substituido por %s\n",r->palavra);
                free(swapp);
                swapl = r->linhas;
                r->linhas = r->dir->linhas;
                free(swapl);
                i = r->dir;
                r->esq = r->dir->esq;
                r->dir = r->dir->dir;
                free(i);
                //printf("%d %d\n",r->esq,r->dir);
            }
        }
        else{
            for(i=r->esq,pi=r; i->dir!=NULL; pi=i,i=i->dir);
            //printf(">> Substituindo por %s\n",i->palavra);
            swapp = r->palavra;
            r->palavra = i->palavra;
            i->palavra = swapp;
            swapl = r->linhas;
            r->linhas = i->linhas;
            i->linhas = swapl;
            //printf(">> Agora, removendo %s\n",i->palavra);
            i = arv_remove(i,i->palavra);
            //printf(">> %s Agora eh pai de um nulo\n",pi->palavra);
            if(pi==r)
                pi->esq = i;
            else
                pi->dir = i;
        }
        return a;
    }
}

arvore_t* arv_destroi(arvore_t* a){
    while(a!=NULL)
        a = arv_remove(a,a->palavra);
    return a;
}

void arv_imprime(arvore_t* a){
    if(a==NULL)
        return;
    arv_imprime(a->esq);
    printf("\n%s : ",a->palavra);
    lis_imprime(a->linhas);
    arv_imprime(a->dir);
}

bool arv_vazia(arvore_t* a){
    if (a==NULL)
        return true;
    return false;
}
