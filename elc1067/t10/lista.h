#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct list{
    int dado;
    struct list *prox, *ant;
}lista_t;

lista_t* lis_cria();

lista_t* lis_insere(lista_t* l, int dado);

void lis_destroi(lista_t* l);

void lis_remove(lista_t* l, int dado);

void lis_imprime(lista_t* l);

bool lis_vazia(lista_t* l);
