#include <stdio.h>
#include <string.h>

struct aluno{
	char nome[60];
	float media;
};

int leArquivo(struct aluno Alunos[]){
	int i,x=0;
	char teste;
	for(i=0;i<50;i++){
		if (scanf("%f", &Alunos[i].media)==EOF)
			break;
		if (Alunos[i].media < 0.0 || Alunos[i].media > 10.0)
			return 0;
		scanf("%[^\n]s",Alunos[i].nome);
		teste = Alunos[i].nome[1];
		if (teste < 'A' || (teste > 'Z' && teste < 'a') || teste > 'z')
			return 0;
		else x++;
	}
	return x;
}

float media(struct aluno Alunos[], int x){
	int i;
	float acum=0;
	for(i=0;i<x;i++)
		acum+=Alunos[i].media;
	return acum/x;
}

void ordena(struct aluno Alunos[], int x){
	int i,j;
	float tempf;
	char temps[60];
	while(1){
		j=0;
		for(i=0;i<x-1;i++)
			if (Alunos[i].media<Alunos[i+1].media){
				tempf = Alunos[i].media;
				Alunos[i].media = Alunos[i+1].media;
				Alunos[i+1].media = tempf;
				
				strcpy(temps,Alunos[i].nome);
				strcpy(Alunos[i].nome,Alunos[i+1].nome);
				strcpy(Alunos[i+1].nome,temps);
				j++;
			}
		if (j==0)
			break;
	}
}

void imprime(struct aluno Alunos[], int x, float m){
	int i;
	printf("%d %.2f\n",x,m);
	for(i=0;i<x;i++)
		printf("%.2f%s\n",Alunos[i].media,Alunos[i].nome);
}

int main (){
	int x;
	struct aluno Alunos[50];
	x = leArquivo(Alunos);
	if (x>0){
		ordena(Alunos, x);
		imprime(Alunos, x, media(Alunos, x));
	}
	else
		printf("Erro!\n\
Arquivo contem dados inconsistentes (fora do padrão) ou está vazio.\n\
Note que o formato aceito pelo programa se dá como:\n\
<nota> <nome>\n\
Esse padrão deve ser repetido em todas as linhas.\n");
	return 0;
}
