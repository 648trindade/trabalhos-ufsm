
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "lista.h"

typedef struct {
    int x;
    int y;
} ponto_t;

bool compara( void *a, void *b )
{
  /* primeiro faz typecast dos ponteiros para ponto_t */
    ponto_t *p1 = (ponto_t*)a;
    ponto_t *p2 = (ponto_t*)b;

    /* compara os pontos aqui */
    if( (p1->x == p2->x) && (p1->y == p2->y) )
        return true;

    return false;
}

int main(int argc, char **argv)
{
    lista_t* lista;
    ponto_t *p1, *p2;

    /* cria a lista */
    lista = lista_cria();

    /* aloca alguns pontos para inserir */
    p1 = (ponto_t*)malloc(sizeof(ponto_t));
    p1->x = p1->y = 1.0;
    p2 = (ponto_t*)malloc(sizeof(ponto_t));
    p2->x = p2->y = 2.0;

    /* insere os dois pontos */
    lista= lista_insere(lista, p1);
    lista= lista_insere(lista, p2);

    /* testa se um deles esta na lista */
    if(lista_busca(lista, p1, compara) == true){
        printf("Hoooray encontrei p1\n");
    } else {
        printf("Errr nao encontrei p1\n");
    }

    /* remove um elemento da lista e testa se ele esta na lista */
    lista = lista_remove(lista, p1, compara);
    if(lista_busca(lista, p1, compara) == true){
        printf("Hoooray encontrei p1\n");
    } else {
        printf("Errr nao encontrei p1\n");
    }

    printf("Tudo Ok\n");

    /* libera memoria */
    lista = lista_remove(lista, p2, compara);
    free(p1);
    free(p2);

    return 0;
}

