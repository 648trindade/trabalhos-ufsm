
#include <stdio.h>
#include <stdlib.h>

#include "lista.h"

/* cria lista vazia */
lista_t* lista_cria(void)
{
    lista_t* l = NULL;
    l = (lista_t*)malloc(sizeof(lista_t));
    l->dado = NULL;
    l->prox = NULL;
    return l;
}

bool lista_busca( lista_t* l, void* dado, bool (*f)(void*, void*)  ){

    if (f){
        if (dado == l->dado)
            return true;
        else if(l->prox != NULL)
            return lista_busca(l->prox, dado, f);
        else{
            return false;
        }
    }
    return false;
}

lista_t* lista_insere( lista_t* l, void* dado ){

    if (l->dado==NULL){
        l->dado = dado;
        l->prox = lista_cria();
    }
    else
        lista_insere(l->prox,dado);
    return l;
}

bool lista_vazia( lista_t* l ){
    if (l->dado == NULL)
        return true;
    else
        return false;
}

lista_t* lista_remove( lista_t* l, void* dado, bool (*f)(void*, void*)  ){
    /*lista_t* ant=NULL, *p = l;
    while (p!=NULL && p->dado!=dado){
        ant = p;
        p = p->prox;
    }
    if(p==NULL)
        return l;
    if(ant==NULL)
        l = p->prox;
    else
        ant->prox = p->prox;
    free(p);
    return l;

    return l;*/
    if (dado==l->dado){
        lista_t *pro = l->prox;
        free(l);
        return pro;
    }
    else if(l->prox!=NULL)
        l->prox = lista_remove(l->prox,dado,f);
    return l;
}
