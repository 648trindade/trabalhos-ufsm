
#ifndef _LISTA_H_
#define _LISTA_H_

#include <stdbool.h>

/* define a TAD lista_t */
typedef struct _lista {
  void* dado;	      /* ponteiro para o dado */
  struct _lista *prox; /* ponteiro para o proximo elemento */
} lista_t;

/* cria uma lista vazia, ou seja, retorna NULL */
lista_t* lista_cria(void);

/* insere no comeco da lista, retorna a lista atualizada */
lista_t* lista_insere( lista_t* l, void* dado );

/* retorna se a lista esta vazia (true), ou false caso contrario */
bool lista_vazia( lista_t* l );

/* busca um elemento na lista por meio da funcao f():
  bool f(void* a, void* b) {}
  que retonar 'true' caso a e b sejam iguais.
*/
bool lista_busca( lista_t* l, void* dado, bool (*f)(void*, void*)  );

/* remove da lista o elemento que contem 'dado'.  Se lista ficar vazia, retorna NULL.
   Se nao encontrou, retorna a lista 'l'.
   Senao, o elemento removido deve ser liberrado com free. ATENCAO: nao liberar o dado.
*/
lista_t* lista_remove( lista_t* l, void* dado, bool (*f)(void*, void*)  );

#endif
