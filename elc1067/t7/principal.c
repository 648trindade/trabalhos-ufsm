
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "geom.h"
#include "tela.h"
#include "vetor.h"

// programa de teste das funcoes geometricas
//
// o programa desenha algumas figuras
// se o mouse passar sobre alguma figura, ela muda de cor
// (se as funcoes ptemret e ptemcirc estiverem corretas)
// pode-se arrastar as figuras com o mouse (botao esquerdo pressionado)
//
// com o botao esquerdo pressionado, as figuras com intersecao mudam de cor
// (se as funcoes de intersecao estiverem funcionando)
//
// apertando a tecla q o programa termina

typedef struct {
  float v;	/* velocidade */
  retangulo_t ret; /* figura */
} laser_t;

typedef struct {
  float v;  /* velocidade */
  retangulo_t ret; /* figura */
} tiro_t;

vetor_t *v = NULL;
vetor_t *tiros = NULL;

/* configuracao inicial do canhao de laser */
laser_t laser;

/* apenas um tiro por vez */
tiro_t *tiro = NULL;

typedef enum direcao {CIMA, BAIXO, DIREITA, ESQUERDA}direcao_t;

/*declara tipo invade*/
typedef struct {
    enum { CIRCULO, RETANGULO } tipo;
    union{
        retangulo_t r;
        circulo_t c;
    }u;
    direcao_t dir; //velocidade
}invader_t;

void inicia_invaders(vetor_t *vet, int n){
    invader_t *inv;
    inv = (invader_t*)malloc(sizeof(invader_t));
    if(rand()%2==0){
        inv->tipo = CIRCULO;
        inv->u.c.raio = rand()%70+10;;
        inv->u.c.centro.y = inv->u.c.raio;
        inv->u.c.centro.x = rand()%(int)((600-2*inv->u.c.raio)+inv->u.c.raio);
    }
    else{
        inv->tipo = RETANGULO;
        inv->u.r.tam.larg = rand()%70+10;
        inv->u.r.tam.alt = rand()%70+10;
        inv->u.r.pos.x = rand()%(int)(600-inv->u.r.tam.larg);
        inv->u.r.pos.y = 0;
    }
    if (rand()%2==0)    inv->dir = ESQUERDA;
    else                inv->dir = DIREITA;
    vetor_insere(vet, n, inv);
}

void tiro_movimenta(void)
{
    int i,cont_end=0;
    tiro_t *bang;
    for(i=0;i<vetor_numelem(tiros);i++){
      if (i >= vetor_numelem(tiros) - cont_end)
        break;
      bang = vetor_acessa(tiros,i);
      if(bang != NULL){
        bang->ret.pos.y -= bang->v;
        /* saiu da tela */
        if(bang->ret.pos.y < 0.0){
          bang = vetor_remove(tiros,i);
          free(bang);
          i--;
          cont_end++;
        }
      }
    }
}

/* lanca tiro do laser */
tiro_t* tiro_fogo(laser_t* l)
{
  tiro_t *t = (tiro_t*)malloc(sizeof(tiro_t));
  t->v = 1.5;
  t->ret.pos.x = l->ret.pos.x;
  t->ret.pos.y = l->ret.pos.y-10;
  t->ret.tam.larg = 10;
  t->ret.tam.alt = 10;
  /* muda estado */
  //estado = fogo;

  return t;
}

void tiro_desenha(tela_t *tela)
{
  int i;
  tiro_t *bang;
  for(i=0;i<vetor_numelem(tiros);i++){
    bang = vetor_acessa(tiros,i);
    if(bang != NULL){
        cor_t preta = {0.0, 0.0, 0.0};
        tela_cor(tela, preta);
        tela_retangulo(tela, bang->ret);
    }
  }
}

void laser_atira(laser_t* l, int tecla)
{
  if(tecla == ALLEGRO_KEY_F)
    vetor_insere(tiros,vetor_numelem(tiros),tiro_fogo(l));
}

void laser_altera_velocidade(laser_t* l, int tecla)
{
  if(tecla == ALLEGRO_KEY_A){
      /* altera velocidade mais a esquerda */
      l->v -= 1;
  } else if(tecla == ALLEGRO_KEY_D) {
      /* altera velocidade mais a direita */
      l->v += 1;
  }
}

void laser_move(tela_t *tela, laser_t* l)
{
  tamanho_t tam = tela_tamanho(tela);
  if(((l->ret.pos.x+l->v) < 0.0) || ((l->ret.pos.x+l->v) > (tam.larg-10)))
    l->v = 0.0;
  l->ret.pos.x += l->v;
}

void laser_desenha(tela_t *tela, laser_t* l)
{
  cor_t vermelho = {1.0, 0.0, 0.0};
  tela_cor(tela, vermelho);
  tela_retangulo(tela, l->ret);
}

void laser_inicia(tela_t *tela)
{
  tamanho_t tam = tela_tamanho(tela);
  laser.v = 0.0;
  laser.ret.pos.x = tam.larg/2;
  laser.ret.pos.y = tam.alt-20;
  laser.ret.tam.larg = 10;
  laser.ret.tam.alt = 20;
  tiros = vetor_cria();
}

void desenha_figuras(tela_t *tela, int tecla){
    cor_t preto = {0, 0, 0};
    tela_cor(tela, preto);
    laser_altera_velocidade(&laser, tecla);
    laser_atira(&laser, tecla);
    laser_desenha(tela, &laser);
    tiro_desenha(tela);
}

void mata_invader(int n){
    if (n!=-1){
        void *r = vetor_remove(v,n);
        free(r);
    }
}

void destroi_invaders(){
    int i;
    for (i=vetor_numelem(v)-1;i>=0;i--)
        mata_invader(i);
    //exit(0);
}

bool acertou_invader(int *vitima){
    invader_t *inv;
    tiro_t *bang;
    int h,i;
    for(h=0;;h++){
        if (h>=vetor_numelem(tiros))
            break;
        bang = vetor_acessa(tiros,h);
        for(i=0;i<vetor_numelem(v);i++){
            inv = vetor_acessa(v,i);
            if (inv->tipo == CIRCULO){
                if (intercr(inv->u.c,bang->ret)){
                    *vitima = i;
                    bang = vetor_remove(tiros,h);
                    free(bang);
                    return true;
                }
            }
            else{
                if (interrr(inv->u.r,bang->ret)){
                    *vitima = i;
                    bang = vetor_remove(tiros,h);
                    free(bang);
                    return true;
                }
            }
        }
    }
    return false;
}

void mostra_invaders(tela_t *tela)
{
    cor_t azul = {0.2, 0.3, 0.8};
    invader_t *inv;
    int i, vitima;
    if (acertou_invader(&vitima))
        mata_invader(vitima);
    for(i=0;i<vetor_numelem(v);i++){
        tela_cor(tela,azul);
        inv = vetor_acessa(v,i);
        if (inv!=NULL){
            if (inv->tipo == CIRCULO)
                tela_circulo(tela,inv->u.c);
            else
                tela_retangulo(tela,inv->u.r);
        }
    }
}

void movimenta_invader_circulo(invader_t *i, float vel, direcao_t dir){
    if (dir == BAIXO){
        i->u.c.centro.y += vel;
        if (i->u.c.raio + i->u.c.centro.y >= 400)
            destroi_invaders();
    }
    if (i->dir == ESQUERDA ){
        i->u.c.centro.x-=vel;
        if (i->u.c.centro.x-i->u.c.raio <=0)
            i->dir = DIREITA;
    }
    else{
        i->u.c.centro.x+=vel;
        if (i->u.c.centro.x+i->u.c.raio >=600)
            i->dir = ESQUERDA;
    }
}

void movimenta_invader_retangulo(invader_t *i, float vel, direcao_t dir){
    if (dir == BAIXO){
        i->u.r.pos.y += vel;
        if (i->u.r.pos.y + i->u.r.tam.alt >= 400)
            destroi_invaders();
    }
    if (i->dir == ESQUERDA ){
        i->u.r.pos.x-=vel;
        if (i->u.r.pos.x-i->u.r.tam.larg <=0)
            i->dir = DIREITA;
    }
    else{
        i->u.r.pos.x+=vel;
        if (i->u.r.pos.x+i->u.r.tam.larg >=600)
            i->dir = ESQUERDA;
    }
}

void move_figuras(tela_t *tela){
    laser_move(tela, &laser);
    tiro_movimenta();
}

bool movimenta_invaders(){
    int i;
    invader_t *invader;
    float vel = 1.0;
    direcao_t dir = BAIXO;

    for (i=0 ;; i++){
        invader = vetor_acessa(v,i);
        if (i>=vetor_numelem(v))
            break;
        if (invader!= NULL){
            if (invader->tipo == CIRCULO)
                movimenta_invader_circulo(invader, vel, dir);
            else
                movimenta_invader_retangulo(invader, vel, dir);
        }
    }
    if (vetor_numelem(v)==0)
        return true;

    return false;
}

int main(int argc, char** argv)
{
    tela_t tela;
    tamanho_t tam = { 600, 400 }; /* tamanho da tela */
    srand(time(NULL));

    int i, n=7;
    v = vetor_cria();
    for (i=0;i<n;i++)
        inicia_invaders(v,i);
    tela_inicializa(&tela, tam, "janela teste");
    int tecla;
    laser_inicia(&tela);
    while((tecla=tela_tecla(&tela)) != ALLEGRO_KEY_Q){
        if (!movimenta_invaders()){
            move_figuras(&tela);
            tela_limpa(&tela);
            desenha_figuras(&tela, tecla);
            mostra_invaders(&tela);
            tela_mostra(&tela);
            tela_espera(30);
        }
        else
            break;
    }
    vetor_destroi(v);
    printf("Invaders completamente removidos. Jogo Finalizado.\n");
    return 0;
}
