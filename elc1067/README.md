# Laboratório de Programação II

**Código:** ELC1067

**Período:** 2014/2

**Professor:** João Vicente F. L.

## Lista de trabalhos

1. Média de Números
2. Campo Minado
3. Campo Minado (final)
4. TAD 1
5. Space Invaders I
6. Space Invaders II
7. Space Invaders III
8. Space Invaders IV
    1. TAD lista
9. Calculadora
    1. Calculadora (Allegro)
10. Ordenação de texto por árvore binária de busca
11. Caminho mais curto em grafos

## Requisitos para compilação
* make
* gcc
* liballegro5-dev