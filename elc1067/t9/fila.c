#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "fila.h"
/* funcoes que permitem realizar operacoes sobre uma fila */

/* retorna uma nova fila, vazia */
fila_t *fila_cria(void){
    fila_t  *l = (fila_t*)malloc(sizeof(fila_t));
    l->prox = NULL;
    l->dado = NULL;
    return l;
}

/* destroi a fila p, que devera estar vazia. */
void fila_destroi(fila_t *l){
    void* dado = fila_remove(l);
    if (dado!=NULL)
        fila_destroi(l);
    //else
        //printf("\n[OK] Fila destruida\n");
}

/* retorna true se a fila p estiver vazia. */
bool fila_vazia(fila_t *l){
    if (l->dado == NULL)
        return true;
    else
        return false;
}

/* insere o dado do tipo void* na fila p */
void fila_insere(fila_t *l, void *dados){
    if (l->dado!=NULL)
        fila_insere(l->prox, dados);
    else{
        l->dado = dados;
        l->prox = fila_cria();
    }
}

/* remove e retorna um elemento na fila */
void *fila_remove(fila_t *l){
    void *ret;
    if (l->dado != NULL){
        fila_t* prox = l->prox;
        ret = l->dado;
        l->dado = prox->dado;
        l->prox = prox->prox;
        free(prox);
    }
    else{
        //printf("\n[Erro] Pilha vazia\n");
        ret = NULL;
    }
    return ret;
}

/* retorna true se p for uma fila válida */
bool fila_valida(fila_t *l){
    return true;
}
