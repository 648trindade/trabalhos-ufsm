#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"

typedef struct {
    struct pilha *operadores;
    struct pilha *operandos; // numero de dados que cabem nessa regiao
    /* outros dados necessarios */
    double res;
} calc_t;

// cria uma nova calculadora, retorna um ponteiro para ela.
calc_t *calc_cria(void){
    calc_t* c = (calc_t*)malloc(sizeof(calc_t));
    c->operadores = NULL;
    c->operandos = NULL;
    c->res = 0;
    return c;
};

// destroi uma calculadora, libera a memoria ocupada por ela.
void calc_destroi(calc_t *c){
    pilha_destroi(c->operadores);
    pilha_destroi(c->operandos);
    free(c);
}

// inicializa a calculadora para novos calculos,
// abandona eventuais calculos pendentes
void calc_inicializa(calc_t *c){
    if (c->operadores!= NULL)
        pilha_destroi(c->operadores);
    if (c->operandos!= NULL)
        pilha_destroi(c->operandos);
    c->operadores = pilha_cria();
    c->operandos = pilha_cria();
}

// realiza um calculo
void calc_calcula(calc_t *c, double val2, bool val2und);

// abre parenteses
void calc_abreparen(calc_t *c){
    char* ins = (char*)malloc(sizeof(char));
    *ins = '(';
    pilha_insere(c->operadores, ins);
}

// fecha parenteses
void calc_fechaparen(calc_t *c){
    while(*((char*)c->operadores->dado)!='(')
          calc_calcula(c,0,true);
    pilha_remove(c->operadores);
    if (!pilha_vazia(c->operandos) && !pilha_vazia(c->operadores))
        if (*((char*)c->operadores->dado)=='*' || *((char*)c->operadores->dado)=='/')
            calc_calcula(c, 0, true);
}

// insere um novo operador para o calculo (pode ser '+', '-', '*', '/', '^')
void calc_operador(calc_t *c, char operador){
    if (operador == '(')
        calc_abreparen(c);
    else if (operador == ')')
        calc_fechaparen(c);
    else{
        char* ins = (char*)malloc(sizeof(char));
        *ins = operador;
        pilha_insere(c->operadores, ins);
    }
}

/*Realiza os calculos e faz as modificações nas pilhas
val2und:    define se val2 precisa ser acessado na pilha ou não*/
void calc_calcula(calc_t *c, double val2, bool val2und){
    double val1, *res;
    char op = *((char*)c->operadores->dado);
    if (val2und){
        if (pilha_vazia(c->operandos->prox)){
            pilha_destroi(c->operandos);
            pilha_destroi(c->operadores);
            c->operadores = pilha_cria();
            c->operandos = pilha_cria();
            calc_operador(c,'(');
            return;
        }
        val2 = *((double*)c->operandos->dado);
        val1 = *((double*)(c->operandos->prox)->dado);
    } else
        val1 = *((double*)c->operandos->dado);
    res = (double*)malloc(sizeof(double));
    /*Testa se o primeiro numero é negativo (precedido de um '-'), se for,
    o negativa e inverte o '-' da pilha pra '+'*/
    if(!pilha_vazia(c->operadores->prox))
        if (*((char*)c->operadores->prox->dado)=='-'){
            char *add = (char*)malloc(sizeof(char));
            *add = '+';
            c->operadores->prox->dado = add;
            val1 = -val1;
        }
    switch(op){
        case '+':
            *res = val1 + val2;  break;
        case '-':
            *res = val1 - val2;  break;
        case '*':
            *res = val1 * val2;  break;
        case '/':
            *res = val1 / val2;  break;
    }
    pilha_remove(c->operadores);
    pilha_remove(c->operandos);
    if (val2und)
        pilha_remove(c->operandos);
    pilha_insere(c->operandos, res);
}

// insere um novo operando para o calculo
void calc_operando(calc_t *c, double operando){
    if (!pilha_vazia(c->operandos) && (*((char*)c->operadores->dado)=='*' || *((char*)c->operadores->dado)=='/'))
        calc_calcula(c, operando, false);
    else{
        double* ins = (double*)malloc(sizeof(double));
        *ins = operando;
        pilha_insere(c->operandos, ins);
    }
}

// retorna o resultado calculo (topo da pilha de calculo)
// esse resultado sera parcial se ainda nao foi chamado calc_fim().
double calc_resultado(calc_t *c){
    return c->res;
}

// finaliza o calculo, retorna false se erro detectado
bool calc_fim(calc_t *c){
    if (pilha_vazia(c->operandos))
        return false;
    else if (!pilha_vazia((c->operandos)->prox) && !pilha_vazia(c->operadores)){
        while (!pilha_vazia(c->operadores) && !pilha_vazia(c->operandos->prox)){
            calc_calcula(c,0,true);
        }
        if (!pilha_vazia(c->operadores) || !pilha_vazia(c->operandos->prox))
            return false;
        else{
            c->res = *((double*)c->operandos->dado);
            return true;
        }
    }
    else if (!pilha_vazia(c->operandos)){
        if (!pilha_vazia(c->operandos->prox) || !pilha_vazia(c->operadores))
            return false;
        else{
            c->res = *((double*)c->operandos->dado);
            return true;
        }
    }
    else
        return false;
}
