#ifndef _FILA_H_
#define _FILA_H_

#include <stdbool.h>

/* definicao do tipo fila */
typedef struct fila{
    void* dado;
    struct fila *prox;
} fila_t;

/* funcoes que permitem realizar operacoes sobre uma fila */

/* retorna uma nova fila, vazia */
fila_t *fila_cria(void);

/* destroi a fila p, que devera estar vazia. */
void fila_destroi(fila_t *p);

/* retorna true se a fila p estiver vazia. */
bool fila_vazia(fila_t *p);

/* insere o dado do tipo void* na fila p */
void fila_insere(fila_t *p, void *dados);

/* remove e retorna o elemento no topo da fila */
void *fila_remove(fila_t *p);

/* retorna true se p for uma fila válida */
bool fila_valida(fila_t *p);

#endif /* _FILA_H_ */

