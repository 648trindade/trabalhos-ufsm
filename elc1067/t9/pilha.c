#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"
/* funcoes que permitem realizar operacoes sobre uma pilha */

/* retorna uma nova pilha, vazia */
pilha_t *pilha_cria(void){
    pilha_t* p = (pilha_t*)malloc(sizeof(pilha_t));
    p->prox = NULL;
    p->dado = NULL;
    return p;
}

/* destroi a pilha p, que devera estar vazia. */
void pilha_destroi(pilha_t *p){
    void* dado = pilha_remove(p);
    if (dado!=NULL)
        pilha_destroi(p);
}

/* retorna true se a pilha p estiver vazia. */
bool pilha_vazia(pilha_t *p){
    if (p->dado == NULL)
        return true;
    else
        return false;
}

/* insere o dado do tipo void* na pilha p */
void pilha_insere(pilha_t *p, void *dados){
    pilha_t* a = (pilha_t*)malloc(sizeof(pilha_t));
    a->dado = p->dado;
    a->prox = p->prox;
    p->dado = dados;
    p->prox = a;
    /*printf("\ninserido %f\n",*((double*)p->dado));
    if (a->dado!=NULL)
        printf("proximo %f\n",*((double*)a->dado));
    else
        printf("proximo NULL\n");
        */
}

/* remove e retorna o elemento no topo da pilha */
void *pilha_remove(pilha_t *p){
    void *ret;
    if (p->dado != NULL){
        pilha_t* prox = p->prox;
        ret = p->dado;
        p->dado = prox->dado;
        p->prox = prox->prox;
        free(prox);
    }
    else{
        //printf("\n[Erro] Pilha vazia\n");
        ret = NULL;
    }
    return ret;
}

/* retorna true se p for uma pilha válida */
bool pilha_valida(pilha_t *p){
    if(p!=NULL){
        return true;
    }
    else{
        return false;
    }
}
