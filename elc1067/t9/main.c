#include <stdio.h>
#include <stdlib.h>
#include "calc.h"
#include <string.h>
#include <stdbool.h>

bool processaArgumento(calc_t* calc, char str[],bool *end){
    double d = 0;
    if (strlen(str)==1){
        char c = str[0];
        if (c=='+' || c=='-' || c=='*' || c=='/' || c=='(' || c==')'){
            calc_operador(calc,c);
            return true;
        }
        else if (c=='='){
            *end = true;
            return false;
        }
        else if (c>='0' && c<='9')
            d = c - 48.0;
        else
            return false;
    }
    else{
        int i, dot = strlen(str);
        double j=1.0;
        for(i=0;i<strlen(str);i++)
            if (str[i]=='.'){
                dot = i;
                break;
            }
        for(i=dot-1;i>=0;i--){
            if ((str[i]<'0'||str[i]>'9') && !(i==0 && str[i]=='-'))
                return false;
            if (i==0 && str[i]=='-')
                d *= -1;
            else{
                d += (str[i]-48)*j;
                j *= 10;
            }
        }
        if (dot!=strlen(str)){
            j=10.0;
            for (i=dot+1;i<strlen(str);i++){
                if (str[i]<'0'||str[i]>'9')
                    return false;
                d += (str[i]-48)/j;
                j *= 10;
            }
        }
    }
    calc_operando(calc, d);
    return true;
}

int main()
{
    calc_t *c;
    float x;
    c = calc_cria();
    char str[10];
    bool end;
    while (1){
        system("clear");
        printf("################ CALCULADORA ################\n");
        printf("# - Insira a operacao desejada              #\n");
        printf("# - Sempre deixe espacos entre as entradas  #\n");
        printf("# - Para encerrar um calculo digite '='     #\n");
        printf("# - Para encerrar a calculadora digite '!'  #\n");
        printf("#############################################\n\n");
        printf("> ");
        calc_inicializa(c);
        end = false;
        while (1){
            scanf("%s",str);
            if (!processaArgumento(c, str, &end))
                break;
        }

        if (end){
            if(calc_fim(c)){
                x = calc_resultado(c);
                if (x- (int)x != 0)
                    printf("Resultado: %.4f\n", x);
                else
                    printf("Resultado: %d\n", (int)x);
            }
            else
                printf("Você inseriu uma operação impossível\n");
        }
        else{
            if (str[0] == '!')
                break;
            else
                printf("Você inseriu valores nao-adequados\n");
        }
        fflush(stdin);
        fflush(stdin);
        printf("\nRealizar outro calculo? (s/n): ");
        scanf("%s",str);
        if (str[0]=='n' || str[0]=='N')
            break;
    }
    calc_destroi(c);
    return 0;
}
