#ifndef _CALC_H_
#define _CALC_H_

#include "pilha.h"

typedef struct {
    pilha_t *operadores;
    pilha_t *operandos; // numero de dados que cabem nessa regiao
    /* outros dados necessarios */
} calc_t;

// cria uma nova calculadora, retorna um ponteiro para ela.
calc_t *calc_cria(void);

// destroi uma calculadora, libera a memoria ocupada por ela.
void calc_destroi(calc_t *c);

// inicializa a calculadora para novos calculos,
// abandona eventuais calculos pendentes
void calc_inicializa(calc_t *c);

// insere um novo operando para o calculo
void calc_operando(calc_t *c, double operando);

// insere um novo operador para o calculo (pode ser '+', '-', '*', '/', '^')
void calc_operador(calc_t *c, char operador);

// abre parenteses
void calc_abreparen(calc_t *c);

// fecha parenteses
void calc_fechaparen(calc_t *c);

// retorna o resultado calculo (topo da pilha de calculo)
// esse resultado sera parcial se ainda nao foi chamado calc_fim().
double calc_resultado(calc_t *c);

// finaliza o calculo, retorna false se erro detectado
bool calc_fim(calc_t *c);

/*
Exemplo de uso do modulo basico:
    calc_t *c;
    double x;

    c = calc_cria();
    calc_operando(c, 1.0);
    calc_operador(x, '+');
    calc_operando(c, 1.0);
    calc_fim(c);
    x = calc_resultado(c);
    printf("1+1=%f\n", x);
    calc_destroi(c);
*/

#endif /* _CALC_H_ */
