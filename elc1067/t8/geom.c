#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "geom.h"


float distancia(ponto_t p1, ponto_t p2)
{
	return sqrt(pow((p2.x-p1.x),2)+pow((p2.y-p1.y),2));
}

bool ptemcirc(ponto_t p, circulo_t c)
{
	if (distancia(p, c.centro)>c.raio)
        return false;
    else
        return true;
}

bool ptemret(ponto_t p, retangulo_t r)
{
	if ((p.x<=r.pos.x+r.tam.larg && p.x>=r.pos.x)&&(p.y<=r.pos.y+r.tam.alt && p.y>=r.pos.y))
        return true;
    else
        return false;
}

bool intercr(circulo_t c, retangulo_t r)
{
	ponto_t ref;
	if (ptemret(c.centro, r))
        return true;
    else{
        if (c.centro.x < r.pos.x)
            ref.x = r.pos.x;
            //ref.x = r.pos.x - c.centro.x;
        else if (c.centro.x > r.pos.x+r.tam.larg)
            ref.x = r.pos.x+r.tam.larg;
            //ref.x = c.centro.x - r.pos.x+r.tam.larg;
        else
            ref.x = c.centro.x;

        if (c.centro.y < r.pos.y)
            ref.y = r.pos.y;
            //ref.y = r.pos.y - c.centro.y;
        else if (c.centro.y > r.pos.y+r.tam.alt)
            ref.y = r.pos.y+r.tam.alt;
            //ref.y = c.centro.y - r.pos.y+r.tam.alt;
        else
            ref.y = c.centro.y;

        if (distancia(c.centro, ref)<c.raio)
            return true;
        else
            return false;
    }
}

bool interrr(retangulo_t r1, retangulo_t r2)
{
	ponto_t _r11,_r12,_r21,_r22;
	_r11.x = r1.pos.x;
	_r11.y = r1.pos.y + r1.tam.alt;
	_r12.x = r1.pos.x + r1.tam.larg;
	_r12.y = r1.pos.y;
	_r21.x = r2.pos.x;
	_r21.y = r2.pos.y + r2.tam.alt;
	_r22.x = r2.pos.x + r2.tam.larg;
	_r22.y = r2.pos.y;

	if (ptemret(r1.pos,r2) || ptemret(r2.pos,r1) || ptemret(_r11,r2) || ptemret(_r21,r1)\
	    || ptemret(_r12,r2) || ptemret(_r22,r1))
        return true;
    else
        return false;
}

bool intercc(circulo_t c1, circulo_t c2)
{
	if (distancia(c1.centro, c2.centro)<=c1.raio+c2.raio)
        return true;
    else
        return false;
}
