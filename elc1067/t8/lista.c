
#include <stdio.h>
#include <stdlib.h>

#include "lista.h"

/* cria lista vazia */
lista_t* lista_cria(void)
{
    lista_t* l = NULL;
    l = (lista_t*)malloc(sizeof(lista_t));
    l->dado = NULL;
    l->prox = NULL;
    return l;
}

int lista_numelem(lista_t* l){
    if(l->dado!=NULL){
        return lista_numelem(l->prox)+1;
    }
    else
        return 0;
}

bool lista_busca( lista_t* l, void* dado){
    if (dado == l->dado)
        return true;
    else if(l->prox != NULL)
        return lista_busca(l->prox, dado);
    else{
        return false;
    }
    return false;
}

void *lista_acessa(lista_t *l, int indice){
    if (l!=NULL){
        if (indice > 0)
            return lista_acessa(l->prox, indice-1);
        else
            return l->dado;
    }
    else{
        return NULL;
    }
}

lista_t* lista_insere( lista_t* l, void* dado ){

    if (l->dado==NULL){
        l->dado = dado;
        l->prox = lista_cria();
    }
    else
        lista_insere(l->prox,dado);
    return l;
}

bool lista_vazia( lista_t* l ){
    if (l->dado == NULL)
        return true;
    else
        return false;
}

lista_t* lista_remove( lista_t* l, void* dado){
    if (dado==l->dado){
        lista_t *pro = l->prox;
        free(l);
        return pro;
    }
    else if(l->prox!=NULL)
        l->prox = lista_remove(l->prox,dado);
    return l;
}

void lista_destroi(lista_t *l){
    while (l!=NULL)
        l = lista_remove(l,l->dado);
    free(l);
}
