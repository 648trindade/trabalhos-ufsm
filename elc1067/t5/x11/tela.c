
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "tela.h"

/* Tamanho da janela */
#define	LARGURA	600
#define ALTURA	400

/* Conversao de unidades do usuario para coordenadas X e vice-versa */
/* Independentemente do tamanho da janela, dizemos que ela tem 80x24 */
//#define XU2X(x) ((short)(LARGURA * (x) / 80))
//#define YU2X(y) ((short)(ALTURA * (y) / 24))
//#define XX2U(x) ((float)(x) / LARGURA * 80)
//#define YX2U(y) ((float)(y) / ALTURA * 24)

/* cada 1 nas coordenadas do usuario corresponde a 10 pixels para o X */
//#define XU2X(x) ((short)((x)*10))
//#define YU2X(y) ((short)((y)*10))
//#define XX2U(x) ((float)(x)/10)
//#define YX2U(y) ((float)(y)/10)
#define XU2X(x) (x)
#define YU2X(y) (y)
#define XX2U(x) (x)
#define YX2U(y) (y)

/* Ajustar de acordo com o servidor X, para acertar as cores */
/* se o servidor X estiver em 16 bits, comentar a linha abaixo */
#define VINTE_E_QUATRO
#ifdef VINTE_E_QUATRO
#define B_R 8
#define B_G 8
#define B_B 8
#else
#define B_R 5
#define B_G 6
#define B_B 5
#endif

/* os eventos que queremos receber do servidor X */
#define EVENT_MASK (ButtonPressMask | ButtonReleaseMask   \
                    | PointerMotionMask                   \
                    | KeyPressMask | KeyReleaseMask)

void tela_inicializa(tela_t* tela, tamanho_t tam, char* nome)
{
    unsigned long valuemask = 0;
    XGCValues values;

    XSetWindowAttributes attr;
    unsigned long mask = 0L;

    /* inicializa dados sobre o mouse */
    tela->rato.x = 0;
    tela->rato.y = 0;
    tela->botao = false;

    tela->tecla = 0;

    /* conecta com servidor X */

    if ((tela->display = XOpenDisplay(NULL)) == NULL) {
        fprintf(stderr, "falha na conexao com servidor X %s\n",
                XDisplayName(NULL));
        exit(-1);
    }

    tela->screen = DefaultScreen(tela->display);

    /* cria a janela */
    attr.event_mask = EVENT_MASK;
    attr.background_pixel = WhitePixel(tela->display, tela->screen);
    attr.border_pixel = WhitePixel(tela->display, tela->screen);
    attr.backing_store = Always;
    attr.save_under = True;
    mask = CWEventMask | CWBackPixel | CWBorderPixel 
         | CWBackingStore | CWSaveUnder;

    tela->tam = tam;
    tela->window = XCreateWindow(tela->display, RootWindow(tela->display, tela->screen),
                           0, 0, tam.larg, tam.alt, 0, 0,
                           InputOutput, (Visual *) CopyFromParent,
                           mask, &attr);
    tela->pixmap = XCreatePixmap(tela->display, RootWindow(tela->display, tela->screen),
                           tam.larg, tam.alt,
                           DefaultDepth(tela->display, tela->screen));

    XStoreName(tela->display, tela->window, nome); /* Titulo da tela */

    /* cria contextos grafico */

    tela->gc_fundo = XCreateGC(tela->display, tela->window, valuemask, &values);
    XSetBackground(tela->display, tela->gc_fundo,
                   WhitePixel(tela->display, tela->screen));
    XSetForeground(tela->display, tela->gc_fundo,
                   WhitePixel(tela->display, tela->screen));

    tela->gc = XCreateGC(tela->display, tela->window, valuemask, &values);
    XSetBackground(tela->display, tela->gc, WhitePixel(tela->display, tela->screen));
    XSetForeground(tela->display, tela->gc, BlackPixel(tela->display, tela->screen));
    XSetLineAttributes(tela->display, tela->gc, 1, LineSolid, CapRound, JoinRound);

    /* mapeia a janela na tela */
    XMapWindow(tela->display, tela->window);
    XSync(tela->display, 0);
}

void tela_finaliza(tela_t* tela)
{
    /* o programa vai morrer, o fim da conexao com o servidor X fecha tudo */
    XFreePixmap(tela->display, tela->pixmap);
    XCloseDisplay(tela->display);
}

void tela_preenche_retangulo(tela_t *tela, retangulo_t r, GC gc)
{
    /* preenche o retangulo r na tela, de acordo com o contexto gc */
    XFillRectangle(tela->display, tela->pixmap, gc, 
                   XU2X(r.pos.x), YU2X(r.pos.y), 
                   XU2X(r.tam.larg), YU2X(r.tam.alt));
}

void tela_limpa(tela_t* tela)
{
    /* preenche um retangulo do tamanho da tela com a cor de fundo */
    ponto_t pos = {0.0, 0.0};
    retangulo_t r = {pos, tela->tam};
    tela_preenche_retangulo(tela, r, tela->gc_fundo);
}

void tela_retangulo(tela_t *tela, retangulo_t r)
{
    /* preenche o retangulo r com a cor padrao */
    tela_preenche_retangulo(tela, r, tela->gc);
}

void tela_circulo(tela_t* tela, circulo_t c)
{
    /* preenche o circulo r na tela com a cor padrao */
    XFillArc(tela->display, tela->pixmap, tela->gc, 
                   XU2X(c.centro.x - c.raio), YU2X(c.centro.y - c.raio), 
                   XU2X(2*c.raio), YU2X(2*c.raio), 0, 360*64);
}

void tela_linha(tela_t *tela, ponto_t p1, ponto_t p2)
{
    /* une os dois pontos com uma linha na cor padrao */
    XDrawLine(tela->display, tela->pixmap, tela->gc, XU2X(p1.x), YU2X(p1.y), 
                                         XU2X(p2.x), YU2X(p2.y));
}

void tela_mostra(tela_t* tela)
{
    /* copia o pixmap com os ultimos desenhos para a janela na tela */
    XCopyArea(tela->display, tela->pixmap, tela->window, tela->gc, 
              0, 0, XU2X(tela->tam.larg), YU2X(tela->tam.alt), 0, 0);
    XFlush(tela->display);
}

#define AJEITA(x) (x < 0 ? 0 : (x > 1 ? 1 : x))
void tela_cor(tela_t* tela, cor_t c)
{
    /* altera a cor padrao */
    int R, G, B, n;
    R = AJEITA(c.r) * ((1<<B_R)-1);
    G = AJEITA(c.g) * ((1<<B_G)-1);
    B = AJEITA(c.b) * ((1<<B_B)-1);
    n = (R<<(B_G+B_B)) | (G<<B_B) | B;
    XSetForeground(tela->display, tela->gc, n);
}

int tela_strlen(tela_t *tela, char *s)
{
    XCharStruct char_struct;
    int z;
    XQueryTextExtents(tela->display, XGContextFromGC(tela->gc), s, strlen(s),
                      &z, &z, &z, &char_struct);
    return char_struct.width;
}

void tela_texto(tela_t *tela, ponto_t p, char *s)
{
    /* escreve o texto s na posicao p da tela */
    XDrawString(tela->display, tela->pixmap, tela->gc, XU2X(p.x), YU2X(p.y), s, strlen(s));
}

void tela_processa_eventos(tela_t *tela)
{
    /* processa eventos do servidor X, atualizando a posicao do mouse
     * e ultima tecla pressionada na variavel da tela-> */
    XEvent evento;
    XButtonEvent *be = (XButtonEvent *) & evento;
    XMotionEvent *me = (XMotionEvent *) & evento;
    XKeyEvent *ke = (XKeyEvent *) & evento;

    while (XCheckMaskEvent(tela->display, EVENT_MASK, &evento) == True) {
        switch (evento.type) {
        case ButtonPress:
            if (be->button == 1) tela->botao = true;
            break;
        case ButtonRelease:
            if (be->button == 1) tela->botao = false;
            break;
        case MotionNotify:
            tela->rato.x = XX2U(me->x);
            tela->rato.y = YX2U(me->y);
            break;
        case KeyPress:
            // se a tecla anterior nao foi lida, e' perdida
            tela->tecla = XKeycodeToKeysym(tela->display, ke->keycode, 0);
            break;
        case KeyRelease:
            break;
        }
    }
}

int tela_tecla(tela_t *tela)
{
    /* retorna a ultima tecla pressionada */
    int tecla;
    tela_processa_eventos(tela);
    tecla = tela->tecla;
    tela->tecla = 0;
    return tecla;
}

ponto_t tela_rato(tela_t *tela)
{
    /* retorna a posicao do mouse */
    tela_processa_eventos(tela);
    return tela->rato;
}

bool tela_botao(tela_t *tela)
{
    /* retorna o estado do botao principal do mouse (true == pressionado) */
    tela_processa_eventos(tela);
    return tela->botao;
}

/* tempo de espera em microsegundos */
void tela_espera(double ms)
{
  usleep(ms*1e3);
}

tamanho_t tela_tamanho(tela_t *tela)
{
    return tela->tam;
}

tamanho_t tela_tamanho_texto(tela_t *t, char *s)
{
    XCharStruct char_struct;
    tamanho_t tam;
    int z;
    XQueryTextExtents(t->display, XGContextFromGC(t->gc), s, strlen(s),
		      &z, &z, &z, &char_struct);
    tam.larg= char_struct.width;
    tam.alt= char_struct.ascent;	// + char_struct.descent;
    return tam;
}

