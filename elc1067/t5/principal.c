
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "geom.h"
#include "tela.h"
#include "vetor.h"

// programa de teste das funcoes geometricas
//
// o programa desenha algumas figuras
// se o mouse passar sobre alguma figura, ela muda de cor
// (se as funcoes ptemret e ptemcirc estiverem corretas)
// pode-se arrastar as figuras com o mouse (botao esquerdo pressionado)
//
// com o botao esquerdo pressionado, as figuras com intersecao mudam de cor
// (se as funcoes de intersecao estiverem funcionando)
//
// apertando a tecla q o programa termina

typedef struct {
  float v;	/* velocidade */
  retangulo_t ret; /* figura */
} laser_t;

typedef struct {
  float v;  /* velocidade */
  retangulo_t ret; /* figura */
} tiro_t;

typedef struct {
    retangulo_t ret;
    enum { normal, com_rato, apertado } estado;
    char *texto;
} botao_t;

botao_t b1;
botao_t b2;

/* estados para o campo texto */
enum { nada, botao1, botao2, fogo } estado;

// as figuras
circulo_t c1 = { {20, 30}, 10};
circulo_t c2 = { {40, 100}, 25};
retangulo_t r1 = { {100, 30}, {40, 22} };
retangulo_t r2 = { {150, 10}, {123, 44} };
ponto_t p1 = {250, 250};
ponto_t p2 = {280, 280};
//rafael
vetor_t *v = NULL;

/* configuracao inicial do canhao de laser */
laser_t laser;

/* apenas um tiro por vez */
tiro_t *tiro = NULL;

void tiro_movimenta(void)
{
  if(tiro != NULL){
    tiro->ret.pos.y -= tiro->v;
    /* saiu da tela */
    if(tiro->ret.pos.y < 0.0){
      free(tiro);
      tiro = NULL;
    }
  }
}

/* lanca tiro do laser */
tiro_t* tiro_fogo(laser_t* l)
{
  tiro_t *t = (tiro_t*)malloc(sizeof(tiro_t));
  t->v = 1.5;
  t->ret.pos.x = l->ret.pos.x;
  t->ret.pos.y = l->ret.pos.y-10;
  t->ret.tam.larg = 10;
  t->ret.tam.alt = 10;
  /* muda estado */
  estado = fogo;

  return t;
}

void tiro_desenha(tela_t *tela)
{
  if(tiro != NULL){
    cor_t preta = {0.0, 0.0, 0.0};
    tela_cor(tela, preta);
    tela_retangulo(tela, tiro->ret);
  }
}

void laser_atira(laser_t* l, int tecla)
{
  if(tecla == ALLEGRO_KEY_F){
    tiro = tiro_fogo(l);
  }
}

void laser_altera_velocidade(laser_t* l, int tecla)
{
  if(tecla == ALLEGRO_KEY_A){
      /* altera velocidade mais a esquerda */
      l->v -= 1;
  } else if(tecla == ALLEGRO_KEY_D) {
      /* altera velocidade mais a direita */
      l->v += 1;
  }
}

void laser_move(tela_t *tela, laser_t* l)
{
  tamanho_t tam = tela_tamanho(tela);
  if(((l->ret.pos.x+l->v) < 0.0) || ((l->ret.pos.x+l->v) > (tam.larg-10)))
    l->v = 0.0;
  l->ret.pos.x += l->v;
}

void laser_desenha(tela_t *tela, laser_t* l)
{
  cor_t vermelho = {1.0, 0.0, 0.0};
  tela_cor(tela, vermelho);
  tela_retangulo(tela, l->ret);
}

void laser_inicia(tela_t *tela)
{
  tamanho_t tam = tela_tamanho(tela);
  laser.v = 0.0;
  laser.ret.pos.x = tam.larg/2;
  laser.ret.pos.y = tam.alt-20;
  laser.ret.tam.larg = 10;
  laser.ret.tam.alt = 20;
}

void botao_desenha(botao_t *b, tela_t *t)
{
    ponto_t be, bd, ce, cd;	// baixo, cima, esquerda, direita
    //cor_t cor_com_rato = {.55, .55, .55};
    cor_t cor_com_rato = {.6, .6, .6};
    cor_t cor_sem_rato = {.5, .5, .5};
    cor_t cor_linha1 = {.3, .3, .3};
    cor_t cor_linha2 = {.7, .7, .7};
    cor_t preto = {0, 0, 0};

    be.x = ce.x = b->ret.pos.x;
    bd.x = cd.x = b->ret.pos.x + b->ret.tam.larg;
    ce.y = cd.y = b->ret.pos.y;
    be.y = bd.y = b->ret.pos.y + b->ret.tam.alt;
    // desenha retangulo
    if (b->estado == com_rato) {
	tela_cor(t, cor_com_rato);
    } else {
	tela_cor(t, cor_sem_rato);
    }
    tela_retangulo(t, b->ret);
    // desenha linhas a esquerda e acima
    if (b->estado == apertado) {
	tela_cor(t, cor_linha1);
    } else {
	tela_cor(t, cor_linha2);
    }
    tela_linha(t, be, ce);
    tela_linha(t, ce, cd);
    // desenha linhas a direita e abaixo
    if (b->estado == apertado) {
	tela_cor(t, cor_linha2);
    } else {
	tela_cor(t, cor_linha1);
    }
    tela_linha(t, be, bd);
    tela_linha(t, bd, cd);

    // desenha texto no meio do botao
    ponto_t pt;
    tamanho_t tt;
    tela_cor(t, preto);
    tt = tela_tamanho_texto(t, b->texto);
    pt.x = b->ret.pos.x + b->ret.tam.larg/ 2 - tt.larg/ 2;
    pt.y = b->ret.pos.y + b->ret.tam.alt/ 2 - tt.alt/ 2;
    if (b->estado == apertado) {
	pt.x++;
	pt.y++;
    }
    tela_texto(t, pt, b->texto);
}

void desenha_campo_texto(tela_t *t)
{
    retangulo_t ret = { { 320, 30 }, { 220, 20 } };
    ponto_t be, bd, ce, cd;	// baixo, cima, esquerda, direita
    cor_t cor_linha1 = {.3, .3, .3};
    cor_t cor_linha2 = {.7, .7, .7};
    cor_t cor_texto = {.7, 0, .2};
    cor_t cor_ret = {1, 1, 1};

    be.x = ce.x = ret.pos.x;
    bd.x = cd.x = ret.pos.x + ret.tam.larg;
    ce.y = cd.y = ret.pos.y;
    be.y = bd.y = ret.pos.y + ret.tam.alt;
    // desenha retangulo
    tela_cor(t, cor_ret);
    tela_retangulo(t, ret);
    // desenha linhas a esquerda e acima
    tela_cor(t, cor_linha1);
    tela_linha(t, be, ce);
    tela_linha(t, ce, cd);
    // desenha linhas a direita e abaixo
    tela_cor(t, cor_linha2);
    tela_linha(t, be, bd);
    tela_linha(t, bd, cd);

    // desenha texto a direita do espaco
    ponto_t pt;
    tamanho_t tt;
    char *texto;
    if (estado == nada)
	texto = "nada, aperte q para sair!";
    if (estado == botao1)
	texto = "apertou botao 1";
    if (estado == botao2)
	texto = "apertou botao 2";
    if (estado == fogo)
	texto = "fogo!";
    tela_cor(t, cor_texto);
    tt = tela_tamanho_texto(t, texto);
    pt.x = ret.pos.x + ret.tam.larg- tt.larg- 4;
    pt.y = ret.pos.y + ret.tam.alt/ 2 - tt.alt/ 2;
    tela_texto(t, pt, texto);
}

void desenha_figuras(tela_t *tela, int tecla)
{
  cor_t azul = {0.2, 0.3, 0.8};
  cor_t vermelho = {1, 0.2, 0};
  cor_t verde = {0.2, 0.9, 0.6};
  cor_t preto = {0, 0, 0};

  b1.estado = normal;
  b2.estado = normal;

  if (tela_botao(tela)) {
    // com o botao pressionado, cada figura e' desenhada em vermelho
    // se houver intersecao com alguma outra ou em azul caso contrario
    if (intercc(c1, c2) || intercr(c1, r1) || intercr(c1, r2) || intercc(*((circulo_t*)vetor_acessa(v,0)), c1) ||\
        intercr(c1, *((retangulo_t*)vetor_acessa(v,1))))
        tela_cor(tela, vermelho);
    else
        tela_cor(tela, azul);
    tela_circulo(tela, c1);
    if (intercc(c2, c1) || intercr(c2, r1) || intercr(c2, r2) || intercc(*((circulo_t*)vetor_acessa(v,0)), c2) ||\
        intercr(c2, *((retangulo_t*)vetor_acessa(v,1))))
        tela_cor(tela, vermelho);
    else
        tela_cor(tela, azul);
    tela_circulo(tela, c2);
    if (intercr(c1, r1) || intercr(c2, r1) || interrr(r1, r2) ||  intercr(*((circulo_t*)vetor_acessa(v,0)),r1) ||\
        interrr(r1, *((retangulo_t*)vetor_acessa(v,1))))
        tela_cor(tela, vermelho);
    else
        tela_cor(tela, azul);
    tela_retangulo(tela, r1);
    if (intercr(c1, r2) || intercr(c2, r2) || interrr(r2, r1) ||  intercr(*((circulo_t*)vetor_acessa(v,0)),r2) ||\
        interrr(r2, *((retangulo_t*)vetor_acessa(v,1))))
        tela_cor(tela, vermelho);
    else
        tela_cor(tela, azul);
    tela_retangulo(tela, r2);
    //rafael
    if (intercc(*((circulo_t*)vetor_acessa(v,0)), c1) || intercc(*((circulo_t*)vetor_acessa(v,0)), c2) ||\
        intercr(*((circulo_t*)vetor_acessa(v,0)), r1) || intercr(*((circulo_t*)vetor_acessa(v,0)), r2) ||\
        intercr(*((circulo_t*)vetor_acessa(v,0)),*((retangulo_t*)vetor_acessa(v,1))))
        tela_cor(tela, vermelho);
    else
        tela_cor(tela, azul);
    tela_circulo(tela, *((circulo_t*)vetor_acessa(v,0)));

    if (intercr(c1, *((retangulo_t*)vetor_acessa(v,1))) || intercr(c2, *((retangulo_t*)vetor_acessa(v,1))) ||\
        interrr(*((retangulo_t*)vetor_acessa(v,1)), r1) || interrr(*((retangulo_t*)vetor_acessa(v,1)), r2) ||\
        intercr(*((circulo_t*)vetor_acessa(v,0)),*((retangulo_t*)vetor_acessa(v,1))))
        tela_cor(tela, vermelho);
    else
        tela_cor(tela, azul);
    tela_retangulo(tela, *((retangulo_t*)vetor_acessa(v,1)));


    /* mouse apertado em um botao */
    if(ptemret(tela_rato(tela), b1.ret)){
      b1.estado = apertado;
      estado = botao1;
    }
    if(ptemret(tela_rato(tela), b2.ret)){
      b2.estado = apertado;
      estado = botao2;
    }
  } else {
    // com o botao solto, cada figura e' desenhada em verde se o mouse estiver
    // sobre ela, ou em azul se nao estiver
    if (ptemcirc(tela_rato(tela), c1)) tela_cor(tela, verde);
    else tela_cor(tela, azul);
    tela_circulo(tela, c1);
    if (ptemcirc(tela_rato(tela), c2)) tela_cor(tela, verde);
    else tela_cor(tela, azul);
    tela_circulo(tela, c2);
    if (ptemret(tela_rato(tela), r1)) tela_cor(tela, verde);
    else tela_cor(tela, azul);
    tela_retangulo(tela, r1);
    if (ptemret(tela_rato(tela), r2)) tela_cor(tela, verde);
    else tela_cor(tela, azul);
    tela_retangulo(tela, r2);

    if (ptemcirc(tela_rato(tela), *((circulo_t*)vetor_acessa(v,0)))) tela_cor(tela, verde);
    else tela_cor(tela, azul);
    tela_circulo(tela, *((circulo_t*)vetor_acessa(v,0)));

    if (ptemret(tela_rato(tela), *((retangulo_t*)vetor_acessa(v,1)))) tela_cor(tela, verde);
    else tela_cor(tela, azul);
    tela_retangulo(tela, *((retangulo_t*)vetor_acessa(v,1)));


    /* mouse encima de um botao */
    if(ptemret(tela_rato(tela), b1.ret))
      b1.estado = com_rato;
    if(ptemret(tela_rato(tela), b2.ret))
      b2.estado = com_rato;
  }
  tela_cor(tela, preto);
  tela_linha(tela, p1, p2);
  tela_linha(tela, p1, *((ponto_t*)vetor_acessa(v,2)));
  tela_linha(tela, *((ponto_t*)vetor_acessa(v,2)), p2);
  botao_desenha(&b1, tela);
  botao_desenha(&b2, tela);
  desenha_campo_texto(tela);

  laser_altera_velocidade(&laser, tecla);
  laser_atira(&laser, tecla);
  laser_desenha(tela, &laser);
  tiro_desenha(tela);
}

void move_figuras(tela_t *tela)
{
  // se o botao estiver pressionado e o mouse estiver sobre alguma figura,
  // troca o centro dessa figura para a posicao do mouse.
  // se o botao nao estiver apertado, ninguem se mexe
  circulo_t *piru = NULL;
  retangulo_t *retg = NULL;
  if (tela_botao(tela)) {
    ponto_t rato;
    rato = tela_rato(tela);
    if (ptemcirc(rato, c1)) {
      c1.centro = rato;
    } else if (ptemcirc(rato, c2)) {
      c2.centro = rato;
    } else if (ptemret(rato, r1)) {
      r1.pos = rato;
      r1.pos.x -= r1.tam.larg / 2;
      r1.pos.y -= r1.tam.alt/2;
    } else if (ptemret(rato, r2)) {
      r2.pos = rato;
      r2.pos.x -= r2.tam.larg / 2;
      r2.pos.y -= r2.tam.alt/2;
    } else if (ptemcirc(rato, *((circulo_t*)vetor_acessa(v,0)))) {
      piru = (circulo_t*)vetor_acessa(v,0);
      piru->centro = rato;
    } else if (ptemret(rato, *((retangulo_t*)vetor_acessa(v,1)))) {
      retg = (retangulo_t*)vetor_acessa(v,1);
      retg->pos = rato;
      retg->pos.x -= retg->tam.larg / 2;
      retg->pos.y -= retg->tam.alt/2;
    }
  }
  laser_move(tela, &laser);
  tiro_movimenta();
}

int main(int argc, char** argv)
{
  tela_t tela;
  tamanho_t tam = { 600, 400 }; /* tamanho da tela */

    // inicializa o botao b1
    b1.ret.pos.x = 100;
    b1.ret.pos.y = 200;
    b1.ret.tam.alt= 20;
    b1.ret.tam.larg= 90;
    b1.texto = "botao 1";
    b1.estado = normal;

    // inicializa o botao b2
    b2.ret.pos.x = 200;
    b2.ret.pos.y = 200;
    b2.ret.tam.alt= 20;
    b2.ret.tam.larg= 90;
    b2.texto = "botao 2";
    b2.estado = normal;

    circulo_t *c = NULL;
    c = (circulo_t*)malloc(sizeof(circulo_t));
    c->centro.x = 50;
    c->centro.y = 50;
    c->raio = 30;
    retangulo_t *r = NULL;
    r = (retangulo_t*)malloc(sizeof(retangulo_t));
    r->pos.x = 60;
    r->pos.y = 60;
    r->tam.larg = 40;
    r->tam.alt = 40;
    ponto_t *otavio = NULL;
    otavio = (ponto_t*)malloc(sizeof(ponto_t));
    otavio->x = 430;
    otavio->y = 200;
    v = vetor_cria();
    vetor_insere(v, 0, c);
    vetor_insere(v, 1, r);
    vetor_insere(v, 2, otavio);

  tela_inicializa(&tela, tam, "janela teste");
  int tecla;
  laser_inicia(&tela);
  while((tecla=tela_tecla(&tela)) != ALLEGRO_KEY_Q){
    move_figuras(&tela);
    tela_limpa(&tela);
    desenha_figuras(&tela, tecla);
    tela_mostra(&tela);
    tela_espera(30);
  }
  return 0;
}
