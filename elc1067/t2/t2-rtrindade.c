#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <locale.h>
#include <string.h>
#include <time.h>

/*LEGENDA:
0 - SEM BOMBA, OCULTO
1 - COM BOMBA, OCULTO
2 - SEM BOMBA, RELEVADO
3 - COM BOMBA, REVELADO
*/
bool imprime(int m, int n, int bomb, char **game){
    system("clear");
    int i,j, contO=0, contB=0;

    printf("LEGENDA:\n\
           * = Oculto\n\
           _ = Vazio\n\
           @ = Bomba\n");

    printf("\n\t");

    //Imprime nros colunas
    for(i=0;i<m;i++)
        printf("%d    ",i+1);

    for(i=0;i<m;i++){
        //imprime nro linha
        printf("\n%d\t",i+1);
        for (j=0;j<n;j++){
            //imprime posição
            if (game[i][j]<'2'){
                printf("*    ");
                contO++;
            }
            else if (game[i][j]=='2')
                printf("_    ");
            else{
                printf("@    ");
                contB++;
            }
        }
    }
    //relatorios
    printf("\n\nBombas encontradas: %d/%d",contB,bomb);
    if (bomb-contB>0)
        printf("\nProbabilidade de encontrar uma bomba: %.2f\%\n",(bomb-contB)*100.0/contO);

    //se encontrou todas as bombas
    if (contB != bomb)     return true;
    else            return false;

}

bool joga(char **game, int m, int n, int i, int j){
    //testa se é um nro valido e se está oculto
    if (i>0 && i<=m && j>0 && j<=n)
        if (game[i-1][j-1]<'2')
            return true;
    return false;
}

void venceu(){
    char com;
    printf("\nVocê Venceu!\nPressione Q para sair ou N para um novo jogo: ");
    while(1){
        scanf(" %c",&com);
        if (com=='q' || com=='Q')
            exit(0);
        else if (com == 'n' || com == 'N')
            break;
    }

}

int main(){

    setlocale(LC_ALL, "Portuguese");

    char **game;
    char com[4];
    int i,j,m,n,b,bomb;
    srand(time(NULL));


    while (1){
        system("clear");

        printf("\nInsira Largura, Comprimento e Qtd. de Bombas: ");
        scanf("%d%d%d",&m,&n,&bomb);
        game = (char **) malloc(m*sizeof(char *));

        for (i=0;i<m;i++){
            game[i] = (char *) malloc(n*sizeof(char *));
            for (j=0;j<n;j++){
                    game[i][j] = '0';
            }
        }

        b=0;
        while(b<bomb){
            i = rand()%m;
            j = rand()%n;
            if (game[i][j]=='0'){
                game[i][j]='1';
                b++;
            }
        }

        while(1){
            if (imprime(m,n,bomb,game)){

                printf("\nPressione Q para sair ou N para um novo jogo\nOu insira a posição (X): ");
                scanf("%s",&com);

                if (strcmp(com,"q")==0 || strcmp(com,"Q")==0)
                    exit(0);

                else if (strcmp(com,"n")==0 || strcmp(com,"N")==0)
                    break;

                else {
                    i = atoi(com);
                    printf("Insira a posição (Y): ");
                    scanf("%d",&j);
                    if (joga(game,m,n,i,j))
                        game[i-1][j-1]+=2;
                }

            }
            else{
                venceu();
                break;
            }
        }
        for (i=0;i<m;i++)
            free(game[i]);
        free(game);
    }

    return 0;
}
