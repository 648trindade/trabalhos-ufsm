
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "geom.h"
#include "tela.h"
#include "vetor.h"

// programa de teste das funcoes geometricas
//
// o programa desenha algumas figuras
// se o mouse passar sobre alguma figura, ela muda de cor
// (se as funcoes ptemret e ptemcirc estiverem corretas)
// pode-se arrastar as figuras com o mouse (botao esquerdo pressionado)
//
// com o botao esquerdo pressionado, as figuras com intersecao mudam de cor
// (se as funcoes de intersecao estiverem funcionando)
//
// apertando a tecla q o programa termina

typedef struct {
  float v;	/* velocidade */
  retangulo_t ret; /* figura */
} laser_t;

typedef struct {
  float v;  /* velocidade */
  retangulo_t ret; /* figura */
} tiro_t;

vetor_t *v = NULL;

/* configuracao inicial do canhao de laser */
laser_t laser;

/* apenas um tiro por vez */
tiro_t *tiro = NULL;

void tiro_movimenta(void)
{
  if(tiro != NULL){
    tiro->ret.pos.y -= tiro->v;
    /* saiu da tela */
    if(tiro->ret.pos.y < 0.0){
      free(tiro);
      tiro = NULL;
    }
  }
}

/* lanca tiro do laser */
tiro_t* tiro_fogo(laser_t* l)
{
  tiro_t *t = (tiro_t*)malloc(sizeof(tiro_t));
  t->v = 1.5;
  t->ret.pos.x = l->ret.pos.x;
  t->ret.pos.y = l->ret.pos.y-10;
  t->ret.tam.larg = 10;
  t->ret.tam.alt = 10;
  /* muda estado */
  //estado = fogo;

  return t;
}

void tiro_desenha(tela_t *tela)
{
  if(tiro != NULL){
    cor_t preta = {0.0, 0.0, 0.0};
    tela_cor(tela, preta);
    tela_retangulo(tela, tiro->ret);
  }
}

void laser_atira(laser_t* l, int tecla)
{
  if(tecla == ALLEGRO_KEY_F){
    tiro = tiro_fogo(l);
  }
}

void laser_altera_velocidade(laser_t* l, int tecla)
{
  if(tecla == ALLEGRO_KEY_A){
      /* altera velocidade mais a esquerda */
      l->v -= 1;
  } else if(tecla == ALLEGRO_KEY_D) {
      /* altera velocidade mais a direita */
      l->v += 1;
  }
}

void laser_move(tela_t *tela, laser_t* l)
{
  tamanho_t tam = tela_tamanho(tela);
  if(((l->ret.pos.x+l->v) < 0.0) || ((l->ret.pos.x+l->v) > (tam.larg-10)))
    l->v = 0.0;
  l->ret.pos.x += l->v;
}

void laser_desenha(tela_t *tela, laser_t* l)
{
  cor_t vermelho = {1.0, 0.0, 0.0};
  tela_cor(tela, vermelho);
  tela_retangulo(tela, l->ret);
}

void laser_inicia(tela_t *tela)
{
  tamanho_t tam = tela_tamanho(tela);
  laser.v = 0.0;
  laser.ret.pos.x = tam.larg/2;
  laser.ret.pos.y = tam.alt-20;
  laser.ret.tam.larg = 10;
  laser.ret.tam.alt = 20;
}

void desenha_figuras(tela_t *tela, int tecla)
{
  cor_t azul = {0.2, 0.3, 0.8};
  cor_t vermelho = {1, 0.2, 0};
  //cor_t verde = {0.2, 0.9, 0.6};
  cor_t preto = {0, 0, 0};

  tela_cor(tela,azul);
  tela_circulo(tela, *((circulo_t*)vetor_acessa(v,0)));
  tela_circulo(tela, *((circulo_t*)vetor_acessa(v,2)));
  tela_retangulo(tela, *((retangulo_t*)vetor_acessa(v,1)));
  tela_retangulo(tela, *((retangulo_t*)vetor_acessa(v,3)));

circulo_t *c = NULL;
retangulo_t *r = NULL;

  if (tiro != NULL){

      if (intercr(*((circulo_t*)vetor_acessa(v,0)),tiro->ret)){
	    c = (circulo_t*)vetor_acessa(v,0);
            c->centro.y = c->raio;
            tela_cor(tela, vermelho);
            tela_circulo(tela, *(c));
	    free(tiro);
            tiro = NULL;
      }

      else if (intercr(*((circulo_t*)vetor_acessa(v,2)),tiro->ret)){
            printf("oi");
            c = (circulo_t*)vetor_acessa(v,2);
            c->centro.y = c->raio;
            tela_cor(tela, vermelho);
            tela_circulo(tela, *(c));
            free(tiro);
            tiro = NULL;
      }
}

if (tiro!=NULL){
      r = (retangulo_t*)vetor_acessa(v,1);
      if (interrr(*(r),tiro->ret)){
            r->pos.y = 0;
            tela_cor(tela, vermelho);
            tela_retangulo(tela, *(r));
            free(tiro);
            tiro = NULL;
      }

      else if (interrr(*((retangulo_t*)vetor_acessa(v,3)),tiro->ret)){
            r = (retangulo_t*)vetor_acessa(v,3);
            r->pos.y = 0;
            tela_cor(tela, vermelho);
            tela_retangulo(tela, *(r));
            free(tiro);
            tiro = NULL;
      }
  }

  tela_cor(tela, preto);

  laser_altera_velocidade(&laser, tecla);
  laser_atira(&laser, tecla);
  laser_desenha(tela, &laser);
  tiro_desenha(tela);
}

void move_figuras(tela_t *tela)
{
  // se o botao estiver pressionado e o mouse estiver sobre alguma figura,
  // troca o centro dessa figura para a posicao do mouse.
  // se o botao nao estiver apertado, ninguem se mexe
  circulo_t *piru = NULL;
  retangulo_t *retg = NULL;

  
  piru = (circulo_t*)vetor_acessa(v,0);
  piru->centro.y += 1;
  if (piru->centro.y + piru->raio >= 400)
    exit(0);

  piru = (circulo_t*)vetor_acessa(v,2);
  piru->centro.y += 1;
  if (piru->centro.y + piru->raio >= 400)
    exit(0);

  retg = (retangulo_t*)vetor_acessa(v,1);
  retg->pos.y += 1;
  if (retg->pos.y + retg->tam.larg >= 400)
    exit(0);

  retg = (retangulo_t*)vetor_acessa(v,3);
  retg->pos.y += 1;
  if (retg->pos.y + retg->tam.larg >= 400)
    exit(0);

  laser_move(tela, &laser);
  tiro_movimenta();
}

int main(int argc, char** argv)
{
  tela_t tela;
  tamanho_t tam = { 600, 400 }; /* tamanho da tela */
  srand(time(NULL));

    circulo_t *c1 = NULL, *c2 = NULL;
    c1 = (circulo_t*)malloc(sizeof(circulo_t));
    c2 = (circulo_t*)malloc(sizeof(circulo_t));
    c1->raio = rand()%70+10;;
    c1->centro.y = c1->raio;
    c1->centro.x = rand()%(int)((tam.larg-2*c1->raio)+c1->raio);
    c2->raio = rand()%70+10;;
    c2->centro.y = c2->raio;
    c2->centro.x = rand()%(int)((tam.larg-2*c2->raio)+c2->raio);

    retangulo_t *r1 = NULL, *r2 = NULL;
    r1 = (retangulo_t*)malloc(sizeof(retangulo_t));
    r2 = (retangulo_t*)malloc(sizeof(retangulo_t));
    r1->tam.larg = rand()%70+10;
    r1->tam.alt = rand()%70+10;
    r1->pos.x = rand()%(int)(tam.larg-r1->tam.larg);
    r1->pos.y = 0;
    r2->tam.larg = rand()%70+10;
    r2->tam.alt = rand()%70+10;
    r2->pos.x = rand()%(int)(tam.larg-r2->tam.larg);
    r2->pos.y = 0;

    v = vetor_cria();
    vetor_insere(v, 0, c1);
    vetor_insere(v, 1, r1);
    vetor_insere(v, 2, c2);
    vetor_insere(v, 3, r2);

  tela_inicializa(&tela, tam, "janela teste");
  int tecla;
  laser_inicia(&tela);
  while((tecla=tela_tecla(&tela)) != ALLEGRO_KEY_Q){
    move_figuras(&tela);
    tela_limpa(&tela);
    desenha_figuras(&tela, tecla);
    tela_mostra(&tela);
    tela_espera(30);
  }
  vetor_destroi(v);
  return 0;
}
