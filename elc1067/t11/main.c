/* 
 * File:   main.c
 * Author: rafael
 *
 * Created on 28 de Novembro de 2014, 17:58
 */

#include <stdio.h>
#include <stdlib.h>
#include "grafo.h"

grafo_t* leArq(char argv[]){
    FILE *f = fopen(argv,"r");
    int i,j;
    fscanf(f,"%d",&i);
    grafo_t *g = grf_cria(i);
    while(fscanf(f,"%d %d",&i,&j)!=EOF)
        grf_insere(g,i,j);
    fclose(f);
    return g;
}

int toInt(char arg[]){
    int l = strlen(arg), r = 0;
    for(l=l-1;l>=0;l--){
        r *= 10;
        r += arg[l]-48;
    }
    return r;
}

int main(int argc, char **argv)
{
    if (argc!=4)
    	printf("Parametros inválidos!\n");
    else
    {
    	grafo_t* grf = leArq(argv[3]);
        grf_caminho_curto(grf,toInt(argv[1]),toInt(argv[2]));
    	grf_destroi(grf);
    }
    return 0;
}

