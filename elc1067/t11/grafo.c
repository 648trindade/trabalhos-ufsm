#include "grafo.h"

grafo_t* grf_cria(int t){
    grafo_t* grf = (grafo_t*)malloc(sizeof(grafo_t));

    grf->vert = lis_cria();
    int i;
    for(i=0;i<t;i++)
        grf->vert = lis_insere(grf->vert, vert_cria(i));
    return grf;
}

void grf_insere(grafo_t* grf, int v1, int v2){
    if(v1>=lis_tamanho(grf->vert) || v2>=lis_tamanho(grf->vert))
        printf("Impossivel inserir a aresta %d -> %d. Um ou mais vert inexistentes.\n",v1,v2);
    else{
        vert_aresta((vertice_t*) lis_get(grf->vert, v1), (vertice_t*) lis_get(grf->vert, v2));
        vert_aresta((vertice_t*) lis_get(grf->vert, v2), (vertice_t*) lis_get(grf->vert, v1));
    }
}

void grf_destroi(grafo_t* grf){
    int i;
    for(i=0;i<lis_tamanho(grf->vert);i++)
        vert_destroi((vertice_t*) lis_get(grf->vert, i));
    lis_destroi(grf->vert);
    free(grf);
}

void grf_caminho_curto(grafo_t* grf, int c, int f){
    vertice_t* com = (vertice_t*) lis_get(grf->vert,c);
    vertice_t* fim = (vertice_t*) lis_get(grf->vert,f);
    char *res = vert_caminho_curto(com,fim);
    printf("%s\n",res);
    free(res);
}
