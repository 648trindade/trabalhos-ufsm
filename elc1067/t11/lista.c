#include "lista.h"

lista_t* lis_cria(){
    return NULL;
}

lista_t* lis_insere(lista_t* l, void* v){
    if (l==NULL){
        l = (lista_t*)malloc(sizeof(lista_t));
        l->vert = v;
        l->prox = NULL;
        l->ant = NULL;
        return l;
    }
    lista_t *i;
    for (i=l;i->prox!=NULL;i=i->prox);
    i->prox = (lista_t*)malloc(sizeof(lista_t));
    i->prox->vert = v;
    i->prox->prox = NULL;
    i->prox->ant = i;
    return l;
}

void lis_destroi(lista_t* l){
    if(l!=NULL){
        lis_destroi(l->prox);
        free(l);
    }
}

bool lis_vazia(lista_t* l){
    if (l==NULL)
        return true;
    return false;
}

int lis_tamanho(lista_t* l){
    if(l!=NULL)
        return lis_tamanho(l->prox) + 1;
    return 0;
}

void* lis_get(lista_t*l, int i){
    if(l==NULL)
        return NULL;
    else if (i==0)
        return l->vert;
    else{
        i--;
        return lis_get(l->prox, i);
    }
}
