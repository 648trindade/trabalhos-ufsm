#include "vertice.h"

vertice_t* vert_cria(int num)
{
    vertice_t* v = (vertice_t*)malloc(sizeof(vertice_t));
    v->num = num;
    v->aresta = lis_cria();
    v->cor = BRANCO;
    return v;
}

void vert_aresta(vertice_t* v, vertice_t* novo)
{
    v->aresta = lis_insere(v->aresta, novo);
}

void vert_destroi(vertice_t* v)
{
    lis_destroi(v->aresta);
    free(v);
}

int toInt2(char arg[]){
    int l = strlen(arg), r = 0;
    for(l=l-1;l>=0;l--){
        r *= 10;
        r += arg[l]-48;
    }
    return r;
}

char* checa_strings(char str[][100], int t, int com){
    char *res = (char*)malloc(100);
    strcpy(res,str[0]);
    for (t=t-1;t>0;t--)
        if(strlen(str[t])<strlen(res))
            strcpy(res,str[t]);
    if(res[1]!=' ' && res[1]>='0' && res[1]<='0'){
        char b[3] = {res[0],res[1],'\0'};
        printf("b %s\n",b);
        if(toInt2(b)!=com)
            strcpy(res,"Não há caminhos possíveis");
    }
    else if(res[0]-48!=com)
            strcpy(res,"Não há caminhos possíveis");
    return res;
}

char* vert_caminho_curto(vertice_t* com, vertice_t* fim){
    if(lis_vazia(com->aresta) || lis_vazia(fim->aresta)){
        char *res = (char*)malloc(100);
        sprintf(res,"Não há caminhos possíveis");
        return res;
    }
    else if(com==fim){
        char *res = (char*)malloc(100);
        sprintf(res,"%d ",com->num);
        //printf("Encontrado! Saindo %d\n",com->num);
        //printf("# Retornando variavel %d\n",res);
        return res;
    }
    else if(fim->cor == PRETO){
        //printf("PRETO! Saindo %d\n",fim->num);
        char *res = (char*)malloc(100);
        sprintf(res,"ENCONTRADO VERTICE PRETO JA ACESSADO");
        //printf("# Retornando variavel %d\n",res);
        return res; //GAMBIARRA PRA DEIXAR A STRING MAIOR
    }
    char path[lis_tamanho(fim->aresta)][100], *res = (char*)malloc(100);
    char *v = (char*)malloc(4), *tmp;
    sprintf(v,"%d ",fim->num);
    fim->cor = PRETO;
    int i;
    //printf("Acessando %s\n",v);
    for(i=0;i<lis_tamanho(fim->aresta);i++){
        strcpy(path[i],"");
        //RECURSIONA
        tmp = vert_caminho_curto(com, lis_get(fim->aresta,i));
        //printf("# free tmp1 %d\n",tmp);
        strcat(path[i], tmp);
        free(tmp);
    }
    fim->cor = BRANCO;
    //tmp = NULL;
    tmp = checa_strings(path, lis_tamanho(fim->aresta), com->num);
    strcpy(res,tmp);
    //printf("# free tmp2 %d\n",tmp);
    strcat(res,v);
    //printf("Saindo %s\n",v);
    //printf("# free v %d\n",v);
    free(v);
    free(tmp);
    return res;
}
