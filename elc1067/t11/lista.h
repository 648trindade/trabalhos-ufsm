#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct list{
    void* vert;
    struct list *prox, *ant;
}lista_t;

lista_t* lis_cria();

lista_t* lis_insere(lista_t* l, void *v);

void lis_destroi(lista_t* l);

bool lis_vazia(lista_t* l);

int lis_tamanho(lista_t* l);

void* lis_get(lista_t*l, int i);
