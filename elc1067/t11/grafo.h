#include "vertice.h"

typedef struct grafo{
    lista_t* vert;
}grafo_t;

grafo_t* grf_cria();

void grf_insere(grafo_t* grf, int v1, int v2);

void grf_destroi(grafo_t* grf);

void grf_caminho_curto(grafo_t* grf, int c, int f);
