#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "lista.h"

typedef enum cor{
    BRANCO,
    CINZA,
    PRETO,
}cor_t;

typedef struct vertice{
    cor_t cor;
    int num, qtdArestas;
    lista_t *aresta;
}vertice_t;

vertice_t* vert_cria(int num);

void vert_aresta(vertice_t* v, vertice_t* novo);

void vert_destroi(vertice_t* v);

char* menor_string(char str[][100], int t);

char* vert_caminho_curto(vertice_t* com, vertice_t* fim);